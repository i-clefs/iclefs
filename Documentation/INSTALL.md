# Installation on Ubuntu 16.04 LTS

## Pre-requisites

### Packages

Install the following packages. As a user **with sudo privileges**, do:

```sh
sudo apt install git libcap-ng-utils wget
```

### NodeJs and NPM

```sh
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs
```

(Those lines are excerpt from https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions )

### Bower

```sh
sudo npm install -g bower
```

###  `iclefs` user

As a user **with sudo privileges**, do:

```sh
sudo useradd -d /home/iclefs -m -s /bin/bash iclefs
```

**Note:** the `iclefs` user is **not** meant to have sudo privileges


### Wakanda server

As a user **with sudo privileges**, do:

```sh
wget https://github.com/Wakanda/wakanda-digital-app-factory/releases/download/v1.1.4/wakanda-community-server_1.1.4_amd64.deb
sudo dpkg -i wakanda-community-server_1.1.4_amd64.deb
```

### Create /var/log/wakanda/

As a user **with sudo privileges**, do:

```sh
sudo mkdir /var/log/wakanda/
sudo chown syslog.wakanda /var/log/wakanda/
sudo chmod 664 /var/log/wakanda/
```

### `iclefs` group

As a user **with sudo privileges**, do:

```sh
sudo usermod -aG iclefs iclefs
sudo usermod -aG iclefs wakanda
```

Note: creating a user `myuser` creates both the user `myuser` and the group `myuser`, thus is it not needed to (re)create the `iclefs` group.

## Installation

### Git clone

As user `iclefs`, do:

```bash
cd /home/iclefs/
git clone https://gitlab.adullact.net/i-clefs/iclefs.git
```

### Chown

As a user **with sudo privileges**, do:

```bash
sudo chown -R wakanda:iclefs /home/iclefs/iclefs
```

### Solution file

As a user **with sudo privileges**, edit the file `/etc/default/wakanda`:

1. uncomment the line starting with `wakanda_opt_solution`
1. give it the full path to file `iclefs` "solution file": (`/home/iclefs/iclefs/IClefs_Solution/IClefs.waSolution`)

### URI

As user `iclefs`, edit file `/home/iclefs/iclefs/IClefs/backend/modules/const.js` and define the exposed IP (name ?) and port:

```javascript
exports.uri = "http://127.0.0.1:8081";
```

**Note to doc writers:** must this IP be public ? (or is there another webserver ?)

### Email address

If you want to change `email address`, edit file `/home/iclefs/iclefs/IClefs/backend/modules/const.js`:

```javascript   
exports.email = "Contact@iclefs.io";
```

### NPM install

```bash
cd /home/iclefs/iclefs/IClefs/web/
npm install
```

### Restart Wakanda service

As a user **with sudo privileges**, do:

```bash
sudo service wakanda restart
```

You can now access the `iclefs` app via the port `8081`.

To start using the app please refer to the `First run` section.

### Installation troubleshooting

In case an HTTP proxy is configured for your local network, you'll need to configure `apt` to take that into account.
You can find more by following this [link](https://askubuntu.com/questions/257290/configure-proxy-for-apt).

### Application logs

You can access Wakanda Server logs through `syslog`. For realtime log  access you can use the following command :

```bash
tail -f /var/log/syslog
```

# First run

In order to access the `iclefs` app we need to create a user with `Super Admin` privileges.
For testing purposes we've added a special URL that does that for you (**This should be disabled in the production version of the app**).

In your browser open the following URL `http://YOUR_DOMAIN/createSuperAdmin`. For a local installation this will be :

```
http://127.0.0.1:8081/createSuperAdmin
```

You can now access the login page and use the following credentials to access the `Super Admin` account :

Login page: [http://127.0.0.1:8081/app/](http://127.0.0.1:8081/app/)

Login: `SuperAdmin@iclefs.io`

Password: `Super Admin`

For usage documentation please refer to the `documentation.md` file.
