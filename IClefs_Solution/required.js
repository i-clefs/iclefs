function myLogin(user, password){
	var collectivityName;
	if (!(currentUser = ds.Account.find("userName == :1", user)))
		return false;
	var keyHa = currentUser.keyHa;
	if (directory.computeHA1(keyHa, password) != currentUser._password){
		return false;
	}
	if (currentUser.status == false) {
		return false;
	}
	currentUser.lastConnection = new Date();
	currentUser.save();
	if(currentUser.role.tab[0] != "Super Admin"){
		collectivityName = currentUser.collectivity.name;
	}else{
		collectivityName = '';
	}
	return {
		ID: currentUser.ID,
		firstName: currentUser.firstName,
		lastName: currentUser.lastName,
		name: currentUser.userName,
		fullName: currentUser.fullName,
		belongsTo: currentUser.role.tab,
		storage:{
			time: currentUser.lastConnection,
			access: collectivityName
		}
	};
}
