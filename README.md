# I-clefs README

## Installation

See [installation documentation](Documentation/INSTALL.md)

## First run

In order to access the `iclefs` app we need to create a user with `Super Admin` privileges.
For testing purposes we've added a special URL that does that for you (**This should be disabled in the production version of the app**).

In your browser open the following URL `http://YOUR_DOMAIN/createSuperAdmin`. For a local installation this will be :

```
http://127.0.0.1:8081/createSuperAdmin
```

You can now access the login page and use the following credentials to access the `Super Admin` account :

Login page: [http://127.0.0.1:8081/app/](http://127.0.0.1:8081/app/)

Login: `SuperAdmin@iclefs.io`

Password: `Super Admin`

For usage documentation please refer to the `documentation.md` file.
