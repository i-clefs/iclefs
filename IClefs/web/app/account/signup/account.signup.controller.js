app.controller("signupController", function ($scope, ds, $location, $state) {

    $scope.emailExist = false;
    $scope.collectiviteExist = false;
    $scope.user = {};

    $scope.createAccount = function (user) {
    	if(user.password === $scope.pw2){
    	    if($scope.user.zipcode.length === 5 && !isNaN($scope.user.zipcode)){
                ds.Account.addUser(user, true).$promise.then(function (event) {
                    if (event.result.status === true) {
                        $state.go('signin', {"requested": true});
                    }
                    else {
                        $scope.msgError = true;
                        $scope.message = event.result.msg;
                    }
                });
            }
            else{
                $scope.msgError = true;
                $scope.message = "Le code postal doit contenir 5 numéros";
            }

    	}        
    }

    $scope.goToLoginPage = function () {
        $location.path("/signin");
    }

    //implement after
     /*   $scope.validate = function($event) {
        var regex = new RegExp("[A-Za-z0-9_ ]");
        var key = String.fromCharCode(!$event.charCode ? $event.which : $event.charCode);
        if (!regex.test(key)) {
            $event.preventDefault();
            return false;
        }
    }*/

    $scope.validateModel = function() {
        var regex = new RegExp(/[^A-Za-z0-9_ ]/);
        if (regex.test($scope.field.name)) {
            $scope.field.name = '';
        }

    }
    //implement after
    /*
    $scope.validateName = function() {
        var regex = new RegExp(/[^A-Za-z0-9_ ]/);
        if (regex.test($scope.mychoice.name)) {
            $scope.mychoice.name = '';
        }
    }*/
    $scope.validateNumber = function ($event) {
        if([8,9,35,36,37,39,46].indexOf($event.keyCode) === -1){

            var regex = new RegExp("[0-9]");
            var key = String.fromCharCode(!$event.charCode ? $event.which : $event.charCode);
            if (!regex.test(key)) {
                $event.preventDefault();
                return false;
            }
            if($scope.user.zipcode && $scope.user.zipcode.length>4){
                $event.preventDefault();
                return false;
            }
        }
     /*   if($event.keyCode !== 8 && $event.keyCode !== 46 && $event.keyCode !== 37 && $event.keyCode !== 39 && $event.keyCode !== 35 && $event.keyCode !== 36 ){
            var regex = new RegExp("[0-9]");
            var key = String.fromCharCode(!$event.charCode ? $event.which : $event.charCode);
            if (!regex.test(key)) {
                $event.preventDefault();
                return false;
            }
            if($scope.user.zipcode && $scope.user.zipcode.length>4){
                $event.preventDefault();
                return false;
            }
        }*/

    }

});