app.controller("forgotPasswordController", function ($window, $scope, ds, $location) {
    $scope.emailNotExist = false;
    $scope.emailExist = false;
    $scope.send_pwd = function () {
        ds.Account.findUserByEmail($scope.email_pwd).$promise.then(function (event) {
            if (event.result.obj === $scope.email_pwd) {
                $scope.emailExist = true;
            }
            else {
                $scope.emailNotExist = true;
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.emailNotExist = false;
                    });
                }, 3000);

            }
        })
    }
    $scope.goToLoginPage = function () {
        $location.path("/login")
    }
});