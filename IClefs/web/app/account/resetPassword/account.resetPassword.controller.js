app.controller("resetPasswordController", function ($stateParams, $scope, ds, $location) {

    initialize();

    function initialize() {
        if (!$stateParams.token_pwd) {
            $location.path("/signin");
        }
        ds.Account.isTokenValid($stateParams.token_pwd).$promise.then(function (event) {
                if (!event.result.status) {
                    $location.path("/signin");
                }	
        });
    }

    $scope.submit_pwd = function () {
        if ($scope.Password != $scope.confirmerPassword) {
            $scope.IsMatch = true;
            setTimeout(function () {
                $scope.$apply(function () {
                    $scope.IsMatch = false;
                });
            }, 3000);
        }
        else {
            ds.Account.findUserByToken($stateParams.token_pwd, $scope.Password).$promise.then(function (event) {
                $scope.pwdRegistred = true;
                setTimeout(function () {
                    $scope.$apply(function () {
                        $location.path("/signin");
                    });
                }, 5000);
            });
        }
    }

});








