app.controller("compteCtrl", function ($rootScope, $scope, ds, $location) {

    initialize();
    $scope.usr;
    $scope.updated = false;
    $scope.error = false;
    $rootScope.page = "Mon compte";
    function initialize() {
        getCurrentUser();
    }

    function getCurrentUser() {
        ds.Account.getCurrentUser().$promise.then(function (res) {
            if (res.result.obj.firstName) {
                $scope.user = res.result.obj;
            }
            else {
                localStorage.clear();
                $location.path('/signin');
            }
        });
    }

    $scope.SaveUserInfo = function (user) {
        ds.Account.updateUser(user).$promise.then(function (res) {
            if (res.result.status == true) {
                $scope.updated = true;
            }
            else {
                $scope.error = true;
            }
        });
    }
    $scope.Cancel = function () {
        $location.path("/administrationStatistics");
    }

});