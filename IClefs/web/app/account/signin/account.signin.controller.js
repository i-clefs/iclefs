app.controller("signinController", function($scope, ds ,$stateParams, $window, $location, $state) {
    $scope.requested = $stateParams.requested;
    $scope.loginFalse = false;
    $scope.account_disabled = false;
    $scope.alerts = [];

    initialize();

    function initialize(){
        testuserSerssion();
    }
    var _message = "";
    $scope.loginFunc = function (user) {
    	$scope.requested = false;
        user.emp = false;
        ds.Account.login(user).$promise.then(function (event) {
            if (event.result.status == true) {

                if(event.result.obj.role != "Super Admin"){
                    localStorage.setItem("userName", event.result.obj.fullName);
                    localStorage.setItem("collectivity", event.result.obj.collectivity);
                    localStorage.setItem("zipcode", event.result.obj.zipcode);
                    localStorage.setItem("role", event.result.obj.role);
                    localStorage.setItem("date",event.result.obj.date);
                    $location.path( "/administrationStatistics" );
                }
                else {
                    localStorage.setItem("userName", event.result.obj.fullName);
                    localStorage.setItem("role", event.result.obj.role);
                    localStorage.setItem("date",event.result.obj.date);
                    $location.path( "/globalStatistics" );
                }
            }
            else {
                if(event.result.msg == "account_disabled")
                    _message = "Votre Compte est désactivé";
                else
                    _message = "Adresse e-mail et/ou mot de passe erronés.";
                $scope.alerts.push({type:'alert-danger',msg: _message});
                $scope.show=true;
                $scope.closeAlert = function(index) {
                    $scope.show = false;
                };
                setTimeout(function(){
                    $scope.$apply(function(){
                        $scope.alerts = [];
                    });
                },2000);
            }
        });
    }

    function testuserSerssion(){
        if(localStorage.userName && localStorage.role){
            if(localStorage.role == "Super Admin") $location.path("/accueil");
            else $location.path("/administrationStatistics");
        }
    }
    $scope.pwd_forgotten = function()
    {
        $state.go("forgotPassword");
    }
    $scope.goCreate = function(){
        $state.go("signup");
    }


});
