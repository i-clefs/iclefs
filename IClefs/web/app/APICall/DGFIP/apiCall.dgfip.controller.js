app.controller("dgfipCtrl", function($scope, ds,  $location, $state, $http, $timeout) {
    $scope.msg = "";
    var serviceID = $location.$$search.state;
    $scope.error = false;
    $scope.loading = false;

    $scope.logDgfip = function(user) {
        $scope.loading = true;
        if (!user || !user.numFiscal || !user.refAvis) {
            $scope.loading = false;
            $scope.error = true;
            $scope.msg = 'veuillez remplir les champs';
        }
        else {
            var url = "/api/v1/dgfip/callback";
            var data = {
                'numFiscal': user.numFiscal,
                'refAvis': user.refAvis,
                'refService': serviceID
            };
            $http.post(url, data).then(successCallback, errorCallback);
        }
    }

    $scope.avort = function() {
        var url = "/api/v1/request/cancelRequest";
        var data = {
            'serviceID' : serviceID
        };
        $http.post(url, data).then(function (result) {
            window.location = result.data.redirectURL;

        });
    }

    function successCallback(res) {
        ds.Services.findServiceByID(serviceID,"dgfip").$promise.then(function (event) {
            if(event.result.obj.page === "caf")
                $state.go('caf', {
                    "srv": event.result.obj.service.ID
                });
            else if(event.result.obj.page === "personalisedData")
                $state.go('personalisedData', {
                    "srv": event.result.obj.service.ID
                });
            else
                $state.go('endRequest', {
                    "srv": event.result.obj.service.ID,
                    "ticket": res.data.ticketID
                });
        });
    }

    function errorCallback(res) {
        $scope.loading = false;
        $scope.error = true;
        $scope.msg = res.data.messageText;
    }

});