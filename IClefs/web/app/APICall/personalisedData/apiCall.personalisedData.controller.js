app.directive('ngFiles', ['$parse', function ($parse) {

    function fn_link(scope, element, attrs) {
        var onChange = $parse(attrs.ngFiles);
        element.on('change', function (event) {
            onChange(scope, {$files: event.target.files});
        });
    };

    return {
        link: fn_link
    }
}])

app.controller("personaliseDataCtrl", function ($stateParams, $scope, ds, $http, $location,  $state) {

    var s_ID = $stateParams.srv || $location.$$search.srv;
    var t_ID = 333333; //$stateParams.ticket || $location.$$search.ticket;
    var personaliseData;
    var files = [];
    var cmpt = 0;
    var stop = 0;
    var fd = new FormData();
    $scope.empty = false;
    var service = null;
    var field
    $scope.showErrorMessage = false;
    $scope.loading = false;


    function initialisation() {
        ds.PersonaliseData.getFields(s_ID).$promise.then(function (event) {
            if (event.result.status) {
                $scope.personaliseData = event.result.obj;
                cmpt = event.result.obj.length;
            }
            else {
                $location.path("/error_service");
            }
        });

        service = ds.Services.findServiceByID(s_ID,"").$promise.then(function (event) {
            $scope.serviceName = event.result.obj.service.name;
        });


    }

    initialisation();


    $scope.getTheFiles = function ($files, fieldID) {
        var name = $("#name_" + fieldID);
        field = fieldID;
        name[0].innerHTML = $files[0].name;
        for (var key in fd.entries()) {
            if (key[0] == "file_" + fieldID) {
                stop--;
                fd.delete(key[0]);
            }
        }
        fd.append("file_" + fieldID, $files[0]);
    };


    // NOW UPLOAD THE FILES.
    $scope.upload = function () {
        $scope.loading = true;
        var url = "/api/v1/service/uploadFile?field=" + field + "&&serviceID=" + s_ID;

        $http.post(url, fd, {
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined
            }
        }).then(function (result) {
            $scope.loading = false;
            var tmp = 0;
            for (var i = 0; i < result.data.length; i++) {
                if (result.data[i].status)
                    tmp++;
            }
            if (result.status != 200) {
                $scope.showErrorMessage = true;
            }
            else {
                $state.go('endRequest', {"srv": s_ID, "ticket": result.data[0].ticketID});
            }

        }, function (result) {
        });

    }
});










