app.controller("cafCtrl", function ($scope, ds,  $http, $location, $state, $stateParams) {
    $scope.loading = false;
    $scope.msg = "";


    var serviceID =  $stateParams.srv || $location.$$search.srv;
    var res = null;
    ds.Services.findServiceByID(serviceID,"caf").$promise.then(function (event) {
        if(event.result.status){
            res = event.result.obj;
            $scope.serviceName = res.service.name;
        }
    })

    $scope.avort = function() {
        var url = "/api/v1/request/cancelRequest";
        var data = {
            'serviceID' : serviceID
        };
        $http.post(url, data).then(function (result) {
            window.location = result.data.redirectURL;

        });
    }
    $scope.logCaf = function (user) {
        $scope.loading = true;
        var data = {};
        data.codePostal = user.codePostal;
        data.numeroAllocataire = user.numAlloc;
        data.refService = serviceID;
        var url = "/api/v1/caf/callback";

        $http.post(url, data).then(function (result) {
            if(res.page === "personalisedData")
                $state.go('personalisedData', {"srv": res.service.ID});
            else
                $state.go('endRequest', {"srv": res.service.ID, "ticket": result.data.ticketID});
        }, function (res) {
            $scope.loading = false;
            $scope.error = true;
            $scope.msg = res.data.messageText;
        });
    };

});