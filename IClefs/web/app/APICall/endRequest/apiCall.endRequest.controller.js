app.controller("finDemandeCtrl", function ($stateParams, $scope, ds, $location) {

    var s_ID = $stateParams.srv || $location.$$search.srv;
    var t_ID = $stateParams.ticket || $location.$$search.ticket;

    var ticket = null;
    var postRedirectUri = null;
    $scope.print = true;
    if ((s_ID.length == 0) || (t_ID.length == 0)) {
        $location.path("/administrationStatistics");
    }

    function initialisation() {
        ds.Services.findServiceByID(s_ID,"endRequest").$promise.then(function (event) {
            var service = event.result.obj.service;
            $scope.serviceName = service.name;
            postRedirectUri = event.result.obj.postRedirectUri;
        });
        ds.Ticket.findTicketByID(t_ID).$promise.then(function (event) {
            ticket = event.result.obj;
            $scope.dateDemande = ticket.date;
            $scope.numDossier = ticket.ID;
            if (ticket.apiData && ticket.apiData.foyerFiscal != undefined)
                $scope.adresse = ticket.apiData.foyerFiscal.adresse;
            var name = "Mme ";
            if (ticket.fcData.gender == "male")
                name = "Mr ";
            name += ticket.fcData.family_name + " " + ticket.fcData.given_name;
            $scope.name = name;


        });
    }

    initialisation();

    $scope.logout = function () {
        if(postRedirectUri == null)
            $location.path("/callback_page");
        else
            window.location = postRedirectUri;
    }
    $scope.printpage = function () {

        //cacher le button pour imprimer l'ecran
        $scope.print = false;


        html2canvas(document.getElementById('recu'), {
            onrendered: function (canvas) {
                var data = canvas.toDataURL();
                var docDefinition = {
                    content: [{
                        image: data,
                        width: 500
                    }]
                };
                pdfMake.createPdf(docDefinition).print();
                $scope.print = true;
                $scope.$apply();
            }

        });


    }
});










