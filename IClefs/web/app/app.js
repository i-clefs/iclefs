
var app = angular.module('starter', ['wakanda', 'ui.router','monospaced.qrcode','ui.bootstrap','ngSanitize','filters','ngMaterial', 'ngAnimate','textAngular'])

angular.module('filters', []).filter('htmlToPlaintext', function() {
    return function(text) {
        return String(text).replace(/<[^>]+>/gm, '');
    }
});

app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    var wakandaManager;
    if(localStorage.userName) {
        $urlRouterProvider.otherwise("/administrationStatistics");
    }
    else {
        $urlRouterProvider.otherwise("/signin");
    }

    $stateProvider
        .state('root', {
            abstract: true,
            template: '<div ui-view=""></div>',
            resolve: {
                ds: function($wakandaManager){
                    wakandaManager = $wakandaManager;
                    return $wakandaManager.getDataStore();
                }
            }
        })
        .state('signin', {
            parent: 'root',
            url: '/signin',
            templateUrl: 'account/signin/account.signin.html',
            controller: 'signinController',
            params: { requested : false}
        })
        .state('forgotPassword',{
            parent: 'root',
            url :'/forgotPassword',
            templateUrl :'account/forgotPassword/account.forgotPassword.html',
            controller : 'forgotPasswordController'
        })
        .state('resetPassword',{
            parent: 'root',
            url :'/resetPassword/:token_pwd',
            templateUrl :'account/resetPassword/account.resetPassword.html',
            controller : 'resetPasswordController'
        })
        .state('signup', {
            parent: 'root',
            url: '/signup',
            templateUrl: 'account/signup/account.signup.html',
            controller: 'signupController'
        })
        .state('401', {
            parent: 'root',
            url: '/401',
            templateUrl: 'views/signin.html',
            controller: 'loginCtrl',
            resolve: {
                access: ["Access", function (Access) { return Access.hasRole("ROLE_ADMIN"); }]
            }
        })
        .state('403', {
            parent: 'root',
            url: '/403',
            templateUrl: 'views/signin.html',
            controller: 'loginCtrl',
        })
        .state('administrationStatistics', {
            parent: 'root',
            url: '/administrationStatistics',
            templateUrl: 'administration/statistics/administration.statistics.html',
            controller: 'administrationStatisticsController',
            resolve: {
                access: ["Access", function (Access) { return Access.isAuthenticated(wakandaManager); }]
            }
        })
        .state('requests', {
            parent: 'root',
            url: '/requests',
            templateUrl: 'administration/requests/administration.requests.html',
            controller: 'requestsController',
            resolve: {
                access: ["Access", function (Access) { return Access.isAuthenticated(wakandaManager); }]
            }
        })
        .state('services', {
            parent: 'root',
            url: '/services',
            templateUrl: 'administration/services/administration.services.html',
            controller: 'servicesController',
            resolve: {
                access: ["Access", function (Access) { return Access.hasRole("Admin de collectivité",wakandaManager); }]
            }
        })
        .state('new_Service', {
            parent: 'root',
            url: '/new_Service',
            templateUrl: 'administration/services/newService/services.newService.html',
            controller: 'createBoutonCtrl',
            resolve: {
                access: ["Access", function (Access) { return Access.hasRole("Admin de collectivité",wakandaManager); }]
            }

        })
        .state('service_inactive', {
            parent: 'root',
            url: '/service_inactive',
            templateUrl: 'administration/services/serviceInactive/service.serviceInactive.html',
            controller: 'serviceInactiveCtrl'

        })
        .state('error_service', {
            parent: 'root',
            url: '/error_service',
            templateUrl: 'administration/services/errorService/services.errorService.html',
            controller: 'errorServiceCtrl'

        })
        .state('callback_page', {
            parent: 'root',
            url: '/callback_page',
            templateUrl: 'administration/collectivity/callbackPage/collectivity.callbackPage.html',
            controller: 'callbackPageCtrl'

        })
        .state('noService', {
            parent: 'root',
            url: '/noService',
            templateUrl: 'administration/services/serviceInactive/service.serviceInactive.html',
            controller: 'serviceInactiveCtrl'

        })
        .state('compte', {
            parent: 'root',
            url: '/compte',
            templateUrl: 'account/update/account.update.html',
            controller: 'compteCtrl',
            resolve: {
                access: ["Access", function (Access) { return Access.isAuthenticated(wakandaManager); }]
            }
        })
        .state('userManagement', {
            parent: 'root',
            url: '/userManagement',
            templateUrl: 'administration/userManagement/administration.userManagement.html',
            controller: 'gestionCtrl',
            resolve: {
                access: ["Access", function (Access) { return Access.hasRole("Admin de collectivité",wakandaManager); }]
            }
        })
        .state('add_user', {
            parent: 'root',
            url: '/add_user',
            templateUrl: 'administration/userManagement/addUser/administration.addUser.html',
            controller: 'add_userCtrl',
            resolve: {
                access: ["Access", function (Access) { return Access.hasRole("Admin de collectivité",wakandaManager); }]
            }
        })
        .state('collectivite', {
            parent: 'root',
            url: '/collectivite',
            templateUrl: 'administration/collectivity/administration.collectivity.html',
            controller: 'collectiviteCtrl',
            resolve: {
                access: ["Access", function (Access) { return Access.hasRole("Admin de collectivité", wakandaManager); }]
            }
        })
        .state('dgfip', {
            parent: 'root',
            url: '/dgfip',
            templateUrl: 'APICall/DGFIP/apiCall.dgfip.html',
            controller: 'dgfipCtrl'
        })
        .state('caf', {
            parent: 'root',
            url: '/caf',
            templateUrl: 'APICall/CAF/apiCall.caf.html',
            params: {
                srv: null,
                ticket: null
            },
            controller: 'cafCtrl'
        })
        .state('globalStatistics', {
            parent: 'root',
            url: '/globalStatistics',
            templateUrl: 'superAdmin/globalStatistics/superAdmin.globalStatistics.html',
            controller: 'accueilAdminCtrl',
            resolve: {
                access: ["Access", function (Access) { return Access.isAuthenticatedSuperAdmin(wakandaManager); }]
            }
        })
        .state('demandes_collectivity', {
            parent: 'root',
            url: '/demandes_collectivity',
            templateUrl: 'superAdmin/requests/superAdmin.requests.html',
            controller: 'demandesCollectiviteCtrl',
            resolve: {
                access: ["Access", function (Access) { return Access.isAuthenticatedSuperAdmin(wakandaManager); }]
            }
        })
        .state('compte_admin', {
            parent: 'root',
            url: '/compte_admin',
            templateUrl: 'superAdmin/account/superAdmin.account.html',
            controller: 'compteSuperAdminCTL',
            resolve: {
                access: ["Access", function (Access) { return Access.isAuthenticatedSuperAdmin(wakandaManager); }]
            }

        })
        .state('collectivity_details', {
            parent: 'root',
            url: '/collectivity_details/:collectivityID',
            templateUrl: 'superAdmin/collectivity/superAdmin.collectivity.html',
            controller: 'detailCollectivityCtrl',
            resolve: {
                access: ["Access", function (Access) { return Access.isAuthenticatedSuperAdmin(wakandaManager); }]
            }

        })
        .state('personalisedData', {
            parent: 'root',
            url: '/personalisedData',
            templateUrl: 'APICall/personalisedData/apiCall.personalisedData.html',
            params: {
                srv: null,
                ticket: null
            },
            controller: 'personaliseDataCtrl'
        })
        .state('endRequest', {
            parent: 'root',
            url: '/endRequest',
            templateUrl: 'APICall/endRequest/apiCall.endRequest.html',
            params: {
                srv: null,
                ticket: null
            },
            controller: 'finDemandeCtrl'
        });

}])

app.factory("Access", ["$q",  function ($q) {

    var Access = {

        OK: 200,

        // "we don't know who you are, so we can't say if you're authorized to access
        // this resource or not yet, please sign in first"
        UNAUTHORIZED: 401,

        // "we know who you are, and your profile does not allow you to access this resource"
        FORBIDDEN: 403,

        hasRole: function (role,wakandaManager) {
            var profileRole = "";
            wakandaManager.getDataStore().$promise.then(function (event) {
                event.Account.getCurrentUser().$promise.then(function (event){
                    profileRole = event.result.obj.role;
                    if(role === profileRole){
                        return Access.OK;
                    }
                    else{
                    	localStorage.clear();
                        window.location = "#!/signin";
                        return $q.reject(Access.UNAUTHORIZED);
                    }


                });
            });
        },

        isAuthenticated: function (wakandaManager) {
            if (localStorage.collectivity && localStorage.userName && localStorage.zipcode && localStorage.role ) {
                wakandaManager.getDataStore().$promise.then(function (event) {
                    event.Account.getCurrentUser().$promise.then(function (event){
                        profileRole = event.result.obj.role;

                        if(profileRole === "Admin de collectivité" || profileRole === "Agent"){
                            return Access.OK;
                        }
                        else{
                        	localStorage.clear();
                            window.location = "#!/signin";
                            return $q.reject(Access.UNAUTHORIZED);
                        }
                    });
                });
            } else {
            	localStorage.clear();
                window.location = "#/signin";
                return $q.reject(Access.UNAUTHORIZED);
            }
        },
        isAuthenticatedSuperAdmin: function (wakandaManager) {
            var profileRole = "";
            wakandaManager.getDataStore().$promise.then(function (event) {
                event.Account.getCurrentUser().$promise.then(function (event) {
                    profileRole = event.result.obj.role;
                    if ((localStorage.userName) && profileRole === "Super Admin") {
                        return Access.OK;
                    } else {
                    	localStorage.clear();
                        window.location = "#/signin";
                        return $q.reject(Access.UNAUTHORIZED);
                    }
                });
            });
        }

    };

    return Access;

}])

