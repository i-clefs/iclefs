app.controller("detailCollectivityCtrl", function ($rootScope, $scope, ds, $stateParams, $location) {

    $rootScope.page = "Collectivité";
    initialisation();


    function initialisation() {
        if ($stateParams.collectivityID == null || $stateParams.collectivityID.length == 0) {
            $location.path("/administrationStatistics")
        }
        else {
            ds.Collectivity.$query({
                filter: 'ID = :1',
                params: [$stateParams.collectivityID]
            }).$promise.then(function (event) {
                var community = event.result[0];
                if (community == null) $location.path("/administrationStatistics");
                $scope.community = community;
                community.accountCollection.$fetch().$promise.then(function (event) {
                    $scope.accounts = event.result;
                });
                community.servicesCollection.$fetch().$promise.then(function (event) {
                    $scope.services = event.result;
                    var ser = event.result;
                })
            })
        }
    }
});