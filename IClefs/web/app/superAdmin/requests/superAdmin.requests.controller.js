app.controller("demandesCollectiviteCtrl", function($rootScope, $scope, ds, $location, modalService, $http) {

    $scope.collectivityNotFound = false;
    $rootScope.page = "Nouvelles collectivités";
    $scope.error = false;
    intialisation();

    //$scope.titles = [ "Action Comics" , "Detective Comics" , "Superman" , "Fantastic Four" , "Amazing Spider-Man" ];

    $scope.detail = function () {
    	if($scope.item.name != "*"){
	        ds.Collectivity.$query({
	            filter: 'name = :1',
	            params: [$scope.item.name]
	        }).$promise.then(function (event) {
	            var collectivity = event.result[0];
	            if(collectivity == null){
	                $scope.collectivityNotFound = true;
	                setTimeout(function(){
	                    $scope.$apply(function(){
	                        $scope.collectivityNotFound = false;
	                    });
	                },3000);
	            }
	            else{
	                $location.path("/collectivity_details/"+collectivity.ID);
	            }

	        });
		}
    }
    // recuperation tous les demandes non valider
    function intialisation() {
        //collectivity active = false
        ds.Collectivity.$query({
            filter: 'active =:1',
            params: [false]
        }).$promise.then(function (event){
            $scope.communitys = event.result;
            for(var i=0;i<event.result.length;i++){
                var tmp = $scope.communitys[i].accountCollection.$fetch().$promise.then(function (ref) {
                    //$scope.communitys[i] = event.result[i];
                });
            }
        })
        //collectivity active = true
        ds.Collectivity.$query({
            filter: 'active =:1',
            params: [true]
        }).$promise.then(function (event){
            $scope.communitysActive = event.result;
        })
    }
    // fin recuperation

    // fonction valider un demande de collectivity
    $scope.valider = function (account,community) {
        ds.Collectivity.valideRefuse(account.ID,community.ID,"valide").$promise.then(function (event) {
            if(event.result.status === true)
                intialisation();
        })
    }
    // fin fonction de validation

    // fonction refuser un demande de collectivity
    $scope.refuser = function (account,community) {

	    var modalOptions = {
	        closeButtonText: 'Annuler',
	        actionButtonText: 'Supprimer',
	        headerText: 'Suppression de la collectivité ?',
	        bodyText: 'le refus de la créeation de la collectivité entrainera sa suppression definitive, Etes vous sur de votre choix ?'
	    };

	    modalService.showModal({}, modalOptions)
	        .then(function (result) {
	            ds.Collectivity.valideRefuse(account.ID,community.ID,"refuse").$promise.then(function (event) {
	           		if(event.result.status === true)
		                intialisation();
		        })
	        });

    }
    // fin fonction refuser

   /* $scope.generateCSV = function () {
        var url = "/api/v1/data/exportCSV";
        var data = {
            'class': "Collectivity",
        };
        $http.post(url, data).then(function (res) {
            //success
            if(res.data.status){
                var blob = new Blob([res.data.obj.data], {
                    type: "data:text/csv;charset=utf-8,"
                });
                saveAs(blob, 'Collectivity.csv');
            }else{
                $scope.msg = res.data.msg;
                $scope.error = true;
                setTimeout(function () {
                    $scope.error = false;
                    $scope.$apply();
                },4000)
            }

        }, function (res) {
            //error
        });
    }*/
});