app.controller("accueilAdminCtrl", function($rootScope, $scope, ds) {

    var positionDay = (new Date()).getDay() - 1;
    var dateNow = new Date();
    var beginOfWeek = new Date(new Date().setDate(dateNow.getDate() - positionDay));
    sessionStorage.setItem("pageName", "Accueil");
    $rootScope.page = "Accueil";
    var ONE_DAY = 60 * 60 * 1000 * 24;
    var ONE_WEEK = ONE_DAY * 7;

    function initialisation() {
        var community = null;

        $scope.totaleDemande = 0;
        $scope.demandeSemain = 0;
        $scope.demandeJour = 0;
        $scope.totaleUsers = 0;
        $scope.userAdmin = 0;
        $scope.userAgent = 0;
        // demandes Collectivity
        ds.Collectivity.$all().$promise.then(function(event) {
                community = event.result;
                for (var i = 0; i < community.length; i++) {
                    var tmp = community[i].accountCollection.$fetch().$promise.then(function(ref) {
                        var dateColl = new Date(ref.result[0].subDate);
                        $scope.totaleDemande++;
                        if ((new Date().getTime() - dateColl.getTime()) < ONE_DAY) {
                            $scope.demandeJour++;
                        }
                        if ((new Date().getTime() - dateColl.getTime()) < ONE_WEEK) {
                            $scope.demandeSemain++;
                        }
                    });;
                }
            })
            // fin demandes Collectivity

        // utilisateur de i-clef
        ds.Account.countAdminAgent().$promise.then(function(event) {
                $scope.userAdmin = event.result.admin;
                $scope.userAgent = event.result.agent;
                $scope.totaleUsers = $scope.userAgent + $scope.userAdmin;
                $scope.totaleCollectivity = event.result.collectivity;
                $scope.totaleService = event.result.service;
                $scope.totaleTicket = event.result.ticket;
            })
            //fin utilisateur i-clef
    }

    initialisation();


});