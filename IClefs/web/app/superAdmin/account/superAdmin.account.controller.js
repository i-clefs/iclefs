app.controller("compteSuperAdminCTL", function ($rootScope, $scope, ds, $location) {

    initialize();
    $scope.user;
    $scope.updated = false;
    $scope.error = false;
    $rootScope.page = "Mon compte";
    function initialize() {
        getCurrentUser();
    }

    function getCurrentUser() {
        ds.Account.getCurrentUser().$promise.then(function (res) {
            $scope.user = res.result.obj;
        });
    }

    $scope.SaveUserInfo = function (user) {
        ds.Account.updateUser(user).$promise.then(function (res) {
            if (res.result.status == true) {
                $scope.error = false;
                $scope.updated = true;
                setTimeout(function () {
                    $scope.$apply(function () {
                        $location.path("/administrationStatistics");
                    });
                }, 3000);

            }
            else {
                $scope.error = true;
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.error = false;
                    });
                }, 3000);
            }
        });
    };

    $scope.Cancel = function () {
        $location.path("/globalStatistics");
    }
});