app.controller("servicesController", function ($rootScope, $scope, $location, ds, $http) {
    $scope.loading = true;
    $scope.created = $location.$$search.created;
    $rootScope.page = "Services France Connect";
    var serviceId;
    $scope.addSuccessfully = false;
    $scope.updateSuccessfully = false;
    $scope.emptyFields = false;
    $scope.error = false;
    $scope.export = '';
    initialize();

    function initialize() {
        $scope.currentPage = 1;
        $scope.nomColl = localStorage.getItem("collectivity")
        ds.Services.testIdSecret($scope.nomColl).$promise.then(function (res) {
            $scope.loading = false;
            if (res.result.status == false) {
                $scope.showButton = false;
            }
            else {
                $scope.showButton = true;

                ds.Services.$query({
                    select: 'ID, status, name, codeBoutton, urlFc, data',
                    filter: 'collectivity.name = :1',
                    params: [localStorage.getItem("collectivity")],
                    pageSize: 5,
                    orderBy: 'creationDate desc'
                }).$promise.then(function (event) {
                    $scope.data = event.result;
                });

            }
        });
    }

    $scope.copy = function (intext) {
        window.clipboardData.setData('Text', intext);
    };

    $scope.deleteIteme = function (thisService) {
        var index = $scope.data.indexOf(thisService);
        thisService.delete();
        $scope.data.splice(index, 1);
        $scope.loading = true;
        initialize();
    };

    $scope.switchStatus = function (thisService) {
        thisService.status = !thisService.status;
        thisService.save();
    };

    $scope.getMail = function (mail) {

        ds.Services.$find(serviceId).$promise.then(function (event) {
            event.result.mailCollection.$fetch().$promise.then(function (event) {
                var mails = event.result;
                var mailSize = mails.length;
                if (mailSize > 0) {
                    for (var i = 0; i < mailSize; i++) {
                        if (mails[i].type == mail.type) {
                            $scope.mail = mails[i];
                        }
                    }
                }

            });
        });

    };

    $scope.saveMail = function (mail) {
        ds.Mail.updateMail(mail, serviceId).$promise.then(function (event) {
            if (event.result.status) {
                if (event.result.msg === "la mise à jour est faite") {
                    $scope.addSuccessfully = true;
                    setTimeout(function () {
                        $scope.$apply(function () {
                            $scope.addSuccessfully = false;
                        });
                    }, 3000);
                }
            } else {
                $scope.emptyFields = true;
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.emptyFields = false;
                    });
                }, 3000);
            }
        })
    };

    $scope.refreshMail = function (service_ID) {
        serviceId = service_ID;
    };

    $scope.closeMail = function () {
        $scope.mail = null;
    }
    new Clipboard("[data-copy-target]", {
        text: function (trigger) {
            $scope.text_copy = document.getElementById(trigger.getAttribute('data-copy-target')).textContent;
            return $scope.text_copy;
        }
    });

    $scope.downloadQRCode = function (id, name) {
        html2canvas(document.getElementById(id), {
            onrendered: function (canvas) {
                var data = canvas.toDataURL();
                var file_path = data;
                var a = document.createElement('A');
                a.href = file_path;
                a.download = name;//"file".substr(file_path.lastIndexOf('/') + 1);
                document.body.appendChild(a);
                a.click();
                document.body.removeChild(a);
            }

        });
    }

    $scope.generateCSV = function () {
        var url = "/api/v1/data/exportCSV";
        var data = {
            'class': $scope.export,
        };
        $http.post(url, data).then(function (res) {
            //success
            if(res.data.status){
                var blob = new Blob([res.data.obj.data], {
                    type: "data:text/csv;charset=utf-8,"
                });
                saveAs(blob, $scope.export+'.csv');

            }else{
                $scope.msg = res.data.msg;
                $scope.error = true;
                setTimeout(function () {
                    $scope.error = false;
                    $scope.$apply();
                },4000)
            }

        }, function (res) {
            //error
        });
    }

});