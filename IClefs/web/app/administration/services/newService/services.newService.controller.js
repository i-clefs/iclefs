app.controller("createBoutonCtrl", function($rootScope, $scope, ds, $location) {


    $scope.errorSaveService;
    $scope.emptyName;
    $scope.showButton = true;
    $scope.subject = "Créer un service France Connect";
    $rootScope.page = "Création des services";
    $scope.emptyField = false;
    $scope.mailExist = false;
    $scope.addSuccessfully = false;
    $scope.emptyMail = false;
    $scope.fields = [];
    $scope.mail = false;
    $scope.loading = false;
    var indice = 0;


    $scope.champs = [];
    $scope.validerChamp = function(champ) {
        $scope.champs.push({
            nom: champ.nom,
            type: champ.type,
            obligatoire: champ.obligatoire
        })
    }
    $scope.france_connect_items = [{
        'id': 'identifiant',
        'name': 'Identité pivot',
        'content': ''
    }];
    //$scope.items = {};
    $scope.items = [{
        'id': 'avisImposition',
        'name': 'Avis d\'imposition',
        'content': 'Contient le revenu fiscal de référence, le nombre de part et les déclarants'
    }, {
        'id': 'adresseFiscale',
        'name': 'Adresse fiscale',
        'content': 'Contient l\'adresse fiscale'
    }];
    $scope.cafs = [{
        'id': 'quotientFamilial',
        'name': 'Quotient familial',
        'content': 'Contient le quotient familial du mois précédent'
    }, {
        'id': 'compositionFamiliale',
        'name': 'Composition familiale',
        'content': 'Contient les parents et les enfants de la famille déclarée à la CAF'
    }, {
        'id': 'adresse',
        'name': 'Adresse',
        'content': 'Contient l\'adresse déclarée à la CAF'
    }];
    $scope.selected = [];
    var mails = [{
        '_type': "acceptation",
        'subject': "Votre demande a été validée",
        'body': " %nomComplet%, <br><br> Votre demande avec le numero de dossier %numDossier% concernant le service %nomService% dans la collectivité  %nomCollectivite%  a été confirmée et validée.",
    }, {
        '_type': "refus",
        'subject': "Votre demande a été refusée",
        'body': " %nomComplet%, <br><br> Votre demande avec le numero de dossier %numDossier% concernant le service %nomService% dans la collectivité %nomCollectivite% a été refusée.<br><br> Nous vous prions de contacter votre collectivité pour plus d'informations.",
    }, {
        '_type': "demande",
        'subject': "Votre demande est en cours de traitement",
        'body': " %nomComplet%, <br><br> Votre demande avec le numero de dossier %numDossier% concernant le service %nomService% dans la collectivité  %nomCollectivite%  a bien été reçue.<br> Nous vous recontacterons le plutôt possible.",
    }];

    //    					------------------------------
    initialize();

    function initialize() {
        $scope.nomColl = localStorage.getItem("collectivity");
        ds.Services.testIdSecret($scope.nomColl).$promise.then(function(res) {
            if (res.result.status == false) {
                $scope.showButton = false;
            }
        });
        $scope.mail = angular.copy(mails[2]);
    }

    $scope.mychoice = {
        name: "",
        data: {
            identifiant: true,
            avisImposition: false,
            adresseFiscale: false,
            quotientFamilial: false,
            compositionFamiliale: false,
            adresse: false
        },
        collectivity: localStorage.getItem("collectivity")
    };

    $scope.addButtonFunc = function(mychoice) {
        if (mychoice.name != "") {
            $scope.loading = true;
            ds.Services.addService(mychoice, $scope.fields, mails).$promise.then(function(res) {
                $scope.loading = false;
                if (res.result.status == true) {
                    $location.path("/services");
                }
                else {
                    $scope.errorSaveService = true;
                }
            });
        }
        else {
            $scope.emptyName = true;
            setTimeout(function() {
                $scope.$apply(function() {
                    $scope.emptyName = false;
                });
            }, 3000);
        }
    }
    $scope.cancelService = function() {
        $location.path("/services");
    }

    /****Add a Field****/
    $scope.addField = function(field) {
        if (field == null) {
            $scope.emptyField = true;
            setTimeout(function() {
                $scope.$apply(function() {
                    $scope.emptyField = false;
                });
            }, 5000);
        }
        else if (field.name == null || field.name.length == 0 || field._type == null) {
            $scope.emptyField = true;
            setTimeout(function() {
                $scope.$apply(function() {
                    $scope.emptyField = false;
                });
            }, 5000);
        }
        else {
            if (field.isRequired != true) field.isRequired = false;
            field.indice = indice;
            indice++;
            $scope.fields.push(field);
            $scope.field = null;
        }
    }

    /******Remove a field*******/
    $scope.removeFields = function(field) {
        var index = $scope.fields.indexOf(field);
        if (index > -1) {
            $scope.fields.splice(index, 1);
        }
    }

    /*****Add mail *****/
    $scope.saveMail = function(mail) {

            if (mail.subject != null && mail.body != null) {
                for (var i = 0; i < 3; i++) {
                    if (mail._type == mails[i]._type) {
                        mails[i] = angular.copy(mail);
                    }
                }
            }
            else {
                $scope.emptyMail = true;
                setTimeout(function() {
                    $scope.$apply(function() {
                        $scope.emptyMail = false;
                    })
                }, 3000);
            }

        }
        /**** get Mail onChange type *****/
        //recuperer mail de Sercive
    $scope.getMail = function(mail) {
        switch (mail._type) {
            case "acceptation":
                $scope.mail = angular.copy(mails[0]);
                break;
            case "refus":
                $scope.mail = angular.copy(mails[1]);
                break;
            case "demande":
                $scope.mail = angular.copy(mails[3]);
                break;
        }
    }
    $scope.validate = function($event) {
        var regex = new RegExp("[A-Za-z0-9_ ]");
        var key = String.fromCharCode(!$event.charCode ? $event.which : $event.charCode);
        if (!regex.test(key)) {
            $event.preventDefault();
            return false;
        }
    }

    $scope.validateModel = function() {
        var regex = new RegExp(/[^A-Za-z0-9_ ]/);
        if (regex.test($scope.field.name)) {
            $scope.field.name = '';
        }

    }
    //implement after
   /* $scope.validateName = function() {
        var regex = new RegExp(/[^A-Za-z0-9_ ]/);
        if (regex.test($scope.mychoice.name)) {
            $scope.mychoice.name = '';
        }
    }*/


});