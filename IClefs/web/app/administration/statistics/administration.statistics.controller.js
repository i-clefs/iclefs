/**
 * Created by chafaiwalid on 04/07/2017.
 */
app.controller("administrationStatisticsController", function ($rootScope, $scope, ds, $http, $location) {

    $scope.fullName = localStorage.getItem("userName");
    $scope.role = localStorage.getItem("role");
    $rootScope.page = localStorage.getItem("collectivity") + ","+localStorage.getItem("zipcode");
    $scope.filterName = "tous";

    var days = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
    var months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Decembre'];
    var now = new Date();
    var day = days[now.getDay()];
    var month = months[now.getMonth()];
    var ONE_DAY = 60 * 60 * 1000 * 24;
    var ONE_WEEK = ONE_DAY * 7;


    $scope.date_fr = day + " " + now.getDate() + " " + month + " " + now.getFullYear();

    $scope.logoutFunc = function () {
        ds.Account.logout().$promise.then(function (res) {
            $location.path("/signin");
            localStorage.clear();
        });
    };

    initialize();

    function initialize() {
        ds.Services.testIdSecret(localStorage.getItem("collectivity")).$promise.then(function (res) {
            if (res.result.status == false) {
                $scope.showButton = false;
            }
            else {
                $scope.showButton = true;
            }
        });

        getStatistics();
        getServices();
    }

    function getStatistics(){
        var rq = {
            filter: 'collectivity.name = :1',
            params: [localStorage.getItem("collectivity")]
        };
        if($scope.filterName != "tous"){
            rq = {
                filter: 'collectivity.name = :1 and name = :2',
                params: [localStorage.getItem("collectivity"), $scope.filterName]
            };
        }
        ds.Services.$query(rq).$promise.then(function (event) {
            var dataService = event.result;
            $scope.data = dataService;
            $scope.all = {dailyState: 0, weeklyState: 0}

            $scope.data.forEach(function (elem) {
                elem.ticket.$fetch().$promise.then(function (tickets) {
                    elem.dailyState = 0;
                    elem.weeklyState = 0;
                    elem.totalState = 0;
                    tickets.result.forEach(function (t) {
                    	elem.totalState++;
                        if (((new Date) - new Date(t.date)) < ONE_DAY) {
                            elem.dailyState++;
                            $scope.all.dailyState++;
                        }
                        if (((new Date) - new Date(t.date)) < ONE_WEEK) {
                            elem.weeklyState++;
                            $scope.all.weeklyState++;
                        }
                    });
                });


            });
        });
    }

    function getServices(){
        $scope.services = ds.Services.$query({
            select: 'name',
            filter: 'collectivity.name = :1',
            params: [localStorage.getItem("collectivity")],
            orderBy: 'ID desc'
        });
    }

    $scope.getStatisticsFilter = function () {
        getStatistics();
    }
    $scope.generateCSV = function () {
        var url = "/api/v1/data/exportCSV";
        var data = {
            'class': "statistics",
        };
        $http.post(url, data).then(function (res) {
            //success
            if(res.data.status){
                var blob = new Blob([res.data.obj.data], {
                    type: "data:text/csv;charset=utf-8,"
                });
                saveAs(blob, 'Statistics.csv');
            }else{
                $scope.msg = res.data.msg;
                $scope.error = true;
                setTimeout(function () {
                    $scope.error = false;
                    $scope.$apply();
                },4000)
            }

        }, function (res) {
            //error
        });
    }
});