app.controller("collectiviteCtrl", function ($rootScope, $scope, ds, $http) {

    $rootScope.page = "Collectivité";
    $scope.data = {};
    $scope.error = false;
    initialize();

    function initialize() {
        getCollectivity();
    }

    function getCollectivity() {
       ds.Collectivity.$query({
           filter: 'name = :1',
           params: [localStorage.getItem("collectivity")]
       }).$promise.then(function (event) {
           if(event.result[0]){
               $scope.data.CNIL = event.result[0].CNIL;
               $scope.data.name = event.result[0].name;
               $scope.data.zipcode = event.result[0].zipCode;
               $scope.data.client_Id = event.result[0].client_Id;
               $scope.data.client_secret = event.result[0].client_secret;
               $scope.data.postRedirectUri = event.result[0].postRedirectUri;
               if(event.result[0].postRedirectUri == null)
                   $scope.data.postRedirectUri = "http://";
           }
           else{

           }
       });
    }

    $scope.setFunc = function (data) {
        if(data.client_Id && data.client_secret){
            if(data.postRedirectUri){
                var code = data.postRedirectUri.indexOf('http://');
                if(data.postRedirectUri.length > "http://".length){
                    if(code == -1){
                        data.postRedirectUri = "http://"+data.postRedirectUri;
                    }
                }
                else {
                    data.postRedirectUri = null;
                }
            }
            else{
                data.postRedirectUri = null;
            }

            ds.Collectivity.$query({
                filter: 'name = :1',
                params: [localStorage.getItem("collectivity")]
            }).$promise.then(function (event) {
                event.result[0].setClient(data).$promise.then(function (event){
                    if (event.result.status == true) {
                        $scope.saved = true;
                        getCollectivity();
                    }
                    else {
                        $scope.saved = false;
                    }
                });
            });
        }
        else{
            $scope.empty = true;
        }
    }
    $scope.setFunc = function (data) {
        if(data.client_Id && data.client_secret){
            if(data.postRedirectUri){
                var code = data.postRedirectUri.indexOf('http://');
                if(data.postRedirectUri.length > "http://".length){
                    if(code == -1){
                        data.postRedirectUri = "http://"+data.postRedirectUri;
                    }
                }
                else {
                    data.postRedirectUri = null;
                }
            }
            else{
                data.postRedirectUri = null;
            }

            ds.Collectivity.$query({
                filter: 'name = :1',
                params: [localStorage.getItem("collectivity")]
            }).$promise.then(function (event) {
                event.result[0].setClient(data).$promise.then(function (event){
                    if (event.result.status == true) {
                        $scope.saved = true;
                        getCollectivity();
                    }
                    else {
                        $scope.saved = false;
                    }
                });
            });
        }
        else{
            $scope.empty = true;
        }
    }

    $scope.generateCSV = function () {
        var url = "/api/v1/data/exportCSV";
        var data = {
            'class': "Collectivity",
        };
        $http.post(url, data).then(function (res) {
            //success
            if(res.data.status){
                var blob = new Blob([res.data.obj.data], {
                    type: "data:text/csv;charset=utf-8,"
                });

                saveAs(blob, 'Collectivity.csv');

            }else{
                $scope.msg = res.data.msg;
                $scope.error = true;
                setTimeout(function () {
                    $scope.error = false;
                    $scope.$apply();
                },4000)
            }
        }, function (res) {
            //error
        });
    }
});