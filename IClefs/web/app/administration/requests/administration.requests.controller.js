app.controller("requestsController", function ($rootScope, $scope, ds, $http, $timeout) {

    $scope.loading = true;
    $scope.Math = window.Math;
    $scope.show = "all";
    $scope.open = false;
    $rootScope.page = "Demandes";
    $scope.filterName = "tous";
    $scope.error = false;
    $scope.export = '';
    initialize();


    function initialize() {
        getServices();
        getTickets();
    }

    function getTickets() {
        setTimeout(function () {
            $scope.nonTreatedRequests();
            $scope.loading = false;
        }, 1500);
    }

    function getServices(){
        $scope.services = ds.Services.$query({
            select: 'name',
            filter: 'collectivity.name = :1',
            params: [localStorage.getItem("collectivity")],
            orderBy: 'ID desc'
        });
    }

    $scope.getTicketFilter = function(){
        getTickets();
    }

    $scope.validate = function (entity) {
        $scope.dataToShow = [];
        $scope.loading = true;
        entity.Validate().then(function () {
            getTickets();
        });
    }

    $scope.refused = function (entity) {
        $scope.dataToShow = [];
        $scope.loading = true;
        entity.refused().then(function () {
            getTickets();
        });
    }

    $scope.allRequests = function () {
        $scope.show = "all";
        var filter = 'collectivity.name = :1';
        var params = [localStorage.getItem("collectivity")];

        if($scope.filterName != "tous"){
            filter = 'collectivity.name = :1 and service.name = :2';
            params = [localStorage.getItem("collectivity"), $scope.filterName];
        }
        getData(filter, params);
    }

    $scope.validatedRequests = function () {
        $scope.show = "validated";
        var filter = 'collectivity.name = :1 and archive = true and status = true';
        var params = [localStorage.getItem("collectivity")];

        if($scope.filterName != "tous"){
            filter = 'collectivity.name = :1 and service.name = :2 and archive = true and status = true';
            params = [localStorage.getItem("collectivity"), $scope.filterName];
        }
        getData(filter, params);
    }

    $scope.nonValidatedRequests = function () {
        $scope.show = "nonValidated";
        var filter = 'collectivity.name = :1 and archive = true and status = false';
        var params = [localStorage.getItem("collectivity")];

        if($scope.filterName != "tous"){
            filter = 'collectivity.name = :1 and service.name = :2 and archive = true and status = false';
            params = [localStorage.getItem("collectivity"), $scope.filterName];
        }
        getData(filter, params);
    }

    $scope.nonTreatedRequests = function () {
        $scope.show = "nonTreated";
        var filter = 'collectivity.name = :1 and archive = false';
        var params = [localStorage.getItem("collectivity")];

        if($scope.filterName != "tous"){
            filter = 'collectivity.name = :1 and service.name = :2 and archive = false';
            params = [localStorage.getItem("collectivity"), $scope.filterName];

        }
        getData(filter,params);
    };

    function getData(filter, params){
        $scope.dataToShow = [];
        $scope.currentPage = 1;
        var rq = {
            select: 'ID, fcData, service, apiData, cafData, dataValueCollection',
            filter: filter,
            params: params,
            pageSize: 10,
            orderBy: 'ID desc'
        };
        $scope.dataToShow = ds.Ticket.$query(rq);
    }

    $scope.generateCSV = function () {
        var url = "/api/v1/data/exportCSV";
        var data = {
            'class': $scope.export,
        };
        $http.post(url, data).then(function (res) {
            //success
            if(res.data.status){
                var blob = new Blob([res.data.obj.data], {
                    type: "data:text/csv;charset=utf-8,"
                });
                saveAs(blob, $scope.export+'.csv');
            }else{
                $scope.msg = res.data.msg;
                $scope.error = true;
                setTimeout(function () {
                    $scope.error = false;
                    $scope.$apply();
                },4000)
            }
        }, function (res) {
            //error
        });
    }
});