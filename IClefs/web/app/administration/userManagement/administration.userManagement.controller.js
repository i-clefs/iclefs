app.controller("gestionCtrl", function ($rootScope, $scope, ds, $location, $http) {

    $scope.userName = localStorage.userName;
    $scope.created = $location.$$search.created;
    $rootScope.page = "Gestion des utilisateurs";
    $scope.error = false;
    $scope.droits = [
        {
            "data1": "Employée mairie",
            "data2": "Employé piscine"
        }
    ];
    $scope.modif = false;
    $scope.cache = "Admin de collectivité";
    ds.Account.$query({
        filter: 'collectivity.name = :1 && fullName != :2',
        params: [localStorage.getItem("collectivity"), localStorage.userName]
    }).$promise.then(function (event) {
        var dataAccount = event.result;
        $scope.data = dataAccount;
    });


    $scope.deleteIteme = function (thisAccount) {
        thisAccount.delete();
        for (i = 0; i < $scope.data.length; i++) {
            if ($scope.data[i].ID == thisAccount.ID) {
                $scope.data.splice($scope.data.indexOf($scope.data[i]), 1);
            }
        }
    };

    $scope.switchStatu = function (thisAccount) {
        thisAccount.status = !thisAccount.status;
        thisAccount.save();
    };
    $scope.switchModif = function () {
        $scope.modif = !$scope.modif;
    };
    $scope.generateCSV = function () {
        var url = "/api/v1/data/exportCSV";
        var data = {
            'class': "Account",
        };
        $http.post(url, data).then(function (res) {
            //success
            if(res.data.status){
                var blob = new Blob([res.data.obj.data], {
                    type: "data:text/csv;charset=utf-8,"
                });
                saveAs(blob, 'Agents.csv');
            }else{
                $scope.msg = res.data.msg;
                $scope.error = true;
                setTimeout(function () {
                    $scope.error = false;
                    $scope.$apply();
                },4000)
            }
        }, function (res) {
            //error
        });
    }
});