app.controller("add_userCtrl", function ($scope, ds, $location) {

    $scope.user = {
        collectivity: localStorage.getItem("collectivity"),
        zipcode: localStorage.getItem("zipcode"),
        emp: true
    };
    $scope.alerts = [];

    $scope.inFunc = function (user) {
        ds.Account.addUser(user).$promise.then(function (res) {
            if (res.result.status == true) {
                //window.location = "#/gestion?created=true";
                $location.path("/userManagement");
            }
            else {
                if (res.result.msg == "This User already exist") {
                    //$scope.emailExist = true;
                    $scope.alerts.push({
                        type: 'alert-danger',
                        msg: 'Un compte est déjà associé à cette adresse email.!'
                    });
                    $scope.show = true;
                    $scope.closeAlert = function (index) {
                        $scope.show = false;
                    };
                    //$scope.user= angular.copy($scope.master);
                }
            }
        });
    }

    $scope.cancelAddUser = function () {
        $location.path("/userManagement");
    }

});