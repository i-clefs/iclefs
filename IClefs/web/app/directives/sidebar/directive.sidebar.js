(function () {
    'use strict';

    angular
        .module('starter')
        .directive('sidebar', sidebar);

    /** @ngInject */
    function sidebar() {
        var directive = {
            restrict: 'E',
            replace: true,
            templateUrl: 'directives/sidebar/directive.sidebar.html',
            controller: sidebarController,
            controllerAs: 'vm',
            bindToController: true
        };
        return directive;


        /** @ngInject */
        function sidebarController($location, $rootScope) {
            var vm = this;
            initialize()
            function initialize() {
                vm.fullName = localStorage.getItem("userName");
                vm.date_fr = Date();
                vm.poste = localStorage.getItem("role");
                vm.nomColl = "Gestion des utilisateurs";
                setsideBarMenu()
            }

            vm.disconnect = function(){
                localStorage.clear();
                localStorage.clear();
                $location.path('/signin');
                //window.location = "#/login";
            }
            function getMenu(name,id) {
                var menus = {
                    menu: [
                        {
                            title: "Accueil",
                            key: "Accueil",
                            active: true,
                            show: true,
                            target: "/#/administrationStatistics",
                            icon: "fa-cloud"
                        },
                        {
                            title: "AccueilAdmin",
                            key: "AccueilAdmin",
                            active: true,
                            target: "/#/globalStatistics",
                            icon: "fa-cloud"
                        },
                        {
                            title: "Statistiques",
                            key: "statistiques",
                            active: false,
                            target: "/#/stat",
                            icon: "fa-cloud"
                        },
                        {
                            title: "Boutons France Connect",
                            key: "BoutonsFranceConnect",
                            active: false,
                            target: "#/services",
                            icon: "fa-cloud"
                        },
                        {
                            title: "Demandes",
                            key: "requests",
                            active: false,
                            target: "/#/requests",
                            icon: "fa-cloud"
                        },
                        {
                            title: "collectivite",
                            key: "collectivite",
                            active: false,
                            target: "/#/collectivite",
                            icon: "fa-cloud"
                        },
                        {
                            title: "Gestion des utilisateurs",
                            key: "Gestiondesutilisateurs",
                            active: false,
                            target: "/#/userManagement",
                            icon: "fa-cloud"
                        },
                        {
                            title: "Mon Compte",
                            key: "MonCompte",
                            active: false,
                            target: "/#/compte",
                            icon: "fa-cloud"
                        },
                        {
                            title: "Mail",
                            key: "MonMail",
                            active: false,
                            target: "/#/build_mail",
                            icon: "fa-cloud"
                        },
                        {
                            title: "Déconnexion",
                            key: "deconnexion",
                            active: false,
                            target: "/#/login",
                            icon: "fa-cloud"
                        },
                        {
                            title: "DemandCollectivity",
                            key: "demendCollectivity",
                            active: false,
                            target: "/#/demandes_collectivity",
                            icon: "fa-cloud"
                        },
                        {
                            title: "compte",
                            key: "compte",
                            active: false,
                            target: "/#/compte_admin",
                            icon: "fa-cloud"
                        }
                    ]
                };
                return menus[name];
            }
            function setsideBarMenu() {
                var path = $location.path();
                $rootScope.accountPage = /^\/new_password/.test(path)|| /^\/signin/.test(path) || /^\/dgfip/.test(path) || /^\/signup/.test(path) || /^\/caf/.test(path) || /^\/forgotPassword/.test(path)|| /^\/admin_login/.test(path)|| /^\/personalisedData/.test(path)|| /^\/endRequest/.test(path) || /^\/service_inactive/.test(path) || /^\/error_service/.test(path) || /^\/callback_page/.test(path) || /^\/resetPassword/.test(path) ;
                vm.menu = getMenu("menu");
                path = "/#" + path;

                vm.menu.map(function (element) {
                    element.active = false;
                    if (path == element.target) {
                        element.active = true;
                    }
                })
            }
            $rootScope.$on("$locationChangeStart", function() {
                setsideBarMenu();
            });
        }

    }

})();
function show_hide() {
    var ul = document.getElementById("navSdebar");
    var icon = document.getElementById("show-menu").classList;
    var event = "block";
    var classe = "glyphicon-triangle-bottom";
    if(ul.style.getPropertyValue("display") == "block"){
        event = "none";
        icon.remove("glyphicon-menu-down");
        icon.add("glyphicon-menu-up");
    }else {
        icon.remove("glyphicon-menu-up");
        icon.add("glyphicon-menu-down");
    }

    ul.style.display = event;

}
