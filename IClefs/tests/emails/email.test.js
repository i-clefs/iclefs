var mail = require("../../backend/modules/sendMail.js");

describe("test module sending mails", function () {
    var body = "body of mail";
    var subject = "email Unit Test";
    var to = "kholti.hamza@gmail.com";

    it("email should be sent", function () {
        this.timeout(5000);
        var result = mail.send(subject,body,to);
        expect(result).to.be.an("object");
        expect(result.status).equal(true);
        expect(result.msg).equal("mail envoyé");
    });

    it("email should not be sent with empty parameters", function () {
        var result = mail.send("","",to);
        expect(result).to.be.an("object");
        expect(result.status).equal(false);
        expect(result.msg).equal("paramètre erroné");
    });

    it("email should not be sent with invalid email address", function () {
        var result = mail.send(subject,body,"emailAddress");
        expect(result).to.be.an("object");
        expect(result.status).equal(false);
        expect(result.msg).equal("Adresse mail non valide");
    });

});