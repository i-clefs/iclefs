var cst = require('const.js');
var superUser = require("../../../backend/Untitled1.js");
var user1 = {
    cnil: 123456,
    collectivity: "myColl",
    firstName: "chafai",
    lastName: "walid",
    password: "123456",
    passwd: "123456",
    userName: "walid.chafai1@wakanda.io",
    zipcode: "10000",
    token_pwd: "passwordToken"
}
var user2 = {
    cnil: 123456,
    collectivity: "myColl2",
    firstName: "chafai",
    lastName: "walid",
    password: "123456",
    userName: "walid.schafai@wakanda.io",
    zipcode: "100500"
}
var agent1 = {
    collectivity: "myColl",
    firstName: "chafai",
    lastName: "walid",
    password: "123456",
    passwd: "123456",
    userName: "agent@wakanda.io",
	emp: true
}
var newCredentials = {
	old : "123456",
	newPassword : "azerty"	
}
var newCredentialsWithfalseold = {
	old : "4532156",
	newPassword : "azerty"	
}
var token ;

describe("Test Account methods", function() {

	this.timeout(15000);
	before(function() {
		token = currentSession().promoteWith(cst.superAdmin);
		//user = ds.Account.query("userName == :1", user1.userName)
        //    if(user[0]) user[0].remove();
	});
    describe("test Add Admin de collectivité", function() {
    	beforeEach(function(){
		    res = ds.Account.addUser(user1,true);
		})
		afterEach(function(){
		    var us1 = ds.Account.query("userName == :1", user1.userName)
		    if(us1[0]) us1[0].remove();
		   
		    var us2 = ds.Account.query("userName == :1", user2.userName)
		  	if(us2[0]) us2[0].remove();
		  	
		  	var cl1 = ds.Collectivity.query("name == :1", user1.collectivity)
		  	if(cl1[0]) cl1[0].remove();
		  	
		  	var cl2 = ds.Collectivity.query("name == :1", user2.collectivity)
		  	if(cl2[0]) cl2[0].remove();
		   
		});
		after(function(){
			res.remove;
		});
        it("should be able to register an admin of collectivity", function() {            
            expect(res.status).to.be.true;
            expect(res.msg).to.equal("userAdd OK");
            expect(res.obj.userName).to.equal(user1.userName);
        });
        it("should not pass because user mail is already in the database", function() {
            var res = ds.Account.addUser(user1,true);
            expect(res.status).to.be.false;
            expect(res.msg).to.equal("Cette adresse email est déjà associée à un compte I-Clefs.");
            expect(res.obj).to.be.empty;
        });
        it("should not pass because user collectity is already in the database", function() {
        	var us1 = ds.Account.query("userName == :1", user1.userName)
		    if(us1[0]) us1[0].remove();
		    var res = ds.Account.addUser(user1,true);
            expect(res.status).to.be.false;
            expect(res.msg).to.equal("Cette Collectivité existe déjà sur I-Clefs.");
            expect(res.obj).to.be.empty;
        });
        it("should not pass because postal code contain 6 caracters", function() {
            res = ds.Account.addUser(user2,true);
            expect(res.status).to.be.false;
            expect(res.msg).to.equal("Le code postal doit contenir 5 numéros");
            expect(res.obj.userName).to.be.empty;
        });
    })

 	describe("test Add Agent", function() {
 		beforeEach(function(){
		    res = ds.Account.addUser(agent1,false);
		})
		afterEach(function(){
		    var user = ds.Account.query("userName == :1", agent1.userName)
            if(user[0]) user[0].remove();
		})
		after(function(){
			res.remove;
		})
        it("should be able to register an agent of collectivity", function() {
            expect(res.status).to.be.true;
            expect(res.msg).to.equal("userAdd OK");
            expect(res.obj.userName).to.equal(agent1.userName);
        });
        it("should not pass because user already exists", function() {
            var res = ds.Account.addUser(agent1,false);
            expect(res.status).to.be.false;
            expect(res.msg).to.equal("Cette adresse email est déjà associée à un compte I-Clefs.");
            expect(res.obj.userName).to.be.empty;
        });
    });
    describe("test login Admin", function() {
    	beforeEach(function(){
		    ds.Account.addUser(user1,true);
		})
		afterEach(function(){
		    var user = ds.Account.query("userName == :1", user1.userName)
            if(user[0]) user[0].remove();
            var coll = ds.Collectivity.query("name == :1", user1.collectivity)
            if (coll[0]) coll[0].remove();
		});
        it("should be not able to connect an admin of collectivity because collectivity is not active", function() {
            var res = ds.Account.login(user1);
            expect(res.status).to.be.false;
            expect(res.msg).to.equal("account_disabled");
            expect(res.obj).to.be.empty;
        });
        it("should be able to connect an admin of collectivity", function() {
            userColl = ds.Collectivity.find("name == :1", user1.collectivity);
            userColl.active = true;
            userColl.save();
            var res = ds.Account.login(user1);
            expect(res.status).to.be.true;
            expect(res.msg).to.equal("login OK");
            expect(res.obj.role).to.equal(cst.admin);
        });
        it("should be not able to connect an admin of collectivity because creadentials are incorrect", function() {
            user1.passwd = "falsePassword"
            var res = ds.Account.login(user1);
            expect(res.status).to.be.false;
            expect(res.msg).to.equal("failed to connect");
            expect(res.obj).to.be.empty;
            user1.passwd = "123456"
        });
        after(function() {
			res.remove;            
        });
    });
    describe("test login Agent", function() {
    	beforeEach(function(){
		    ds.Account.addUser(user1,true);
		    ds.Account.addUser(agent1,false);
		})
		afterEach(function(){
		    var user = ds.Account.query("userName == :1", user1.userName)
            if(user[0]) user[0].remove();
            var agt = ds.Account.query("userName == :1", agent1.userName)
            if(agt[0]) agt[0].remove();
            var coll = ds.Collectivity.query("name == :1", user1.collectivity)
            if (coll[0]) coll[0].remove();
		});
		
        it("should be not able to connect an agent of collectivity because collectivity is not active", function() {
            var res = ds.Account.login(agent1);
            expect(res.status).to.be.false;
            expect(res.msg).to.equal("account_disabled");
            expect(res.obj).to.be.empty;
        });
        it("should be able to connect an agent of collectivity", function() {
            userColl = ds.Collectivity.find("name == :1", agent1.collectivity);
            userColl.active = true;
            userColl.save();
            var res = ds.Account.login(agent1);
            expect(res.status).to.be.true;
            expect(res.msg).to.equal("login OK");
            expect(res.obj.role).to.equal(cst.agent);
        });
        it("should be not able to connect an agent of collectivity because creadentials are incorrect", function() {
            agent1.passwd = "falsePassword"
            var res = ds.Account.login(agent1);
            expect(res.status).to.be.false;
            expect(res.msg).to.equal("failed to connect");
            expect(res.obj).to.be.empty;
            agent1.passwd = "123456"
        });
    });
    describe("test current user Admin de collectivité", function() {
        it("should not find current user because no one is connected", function() {
        	logout();
        	var res = ds.Account.getCurrentUser();
            expect(res.status).to.be.false;
            expect(res.msg).to.equal("");
            expect(res.obj).to.be.empty;
        });
        it("should return current user admin of collectivity", function() {
            ds.Account.addUser(user1,true);
            ds.Account.login(user1);
            var res = ds.Account.getCurrentUser();
            expect(res.status).to.be.true;
            expect(res.msg).to.equal("login OK");
            expect(res.obj.role).to.equal(cst.admin);
        });
        after(function() {
            user = ds.Account.query("userName == :1", user1.userName)
            if(user[0]) user[0].remove();
            coll = ds.Collectivity.query("name == :1", user1.collectivity)
            if (coll[0]) coll[0].remove();
            res.remove;
        });
    });
    describe("test current user Agent", function() {
        it("should return current user Agent", function() {
            ds.Account.addUser(user1,true);
            ds.Account.addUser(agent1,false);
            ds.Account.login(agent1);
            res = ds.Account.getCurrentUser();
            expect(res.status).to.be.true;
            expect(res.msg).to.equal("login OK");
            expect(res.obj.role).to.equal(cst.agent);
        });
        after(function() {
        	coll = ds.Collectivity.query("name == :1", user1.collectivity)
            if (coll[0]) coll[0].remove();
            user = ds.Account.query("userName == :1", user1.userName)
            if(user[0]) user[0].remove();
            user = ds.Account.query("userName == :1", agent1.userName)
            if(user[0]) user[0].remove();
            ds.Account.logout();
        });
    });
    describe("test update user credentials", function() {
        it("should not pass because there is no user logged in", function() {
            var res = ds.Account.updateUser(newCredentials);
            expect(res.status).to.be.false;
            expect(res.msg).to.equal("There is no user logged in");
            expect(res.obj.role).to.be.empty;
        });
        it("should not pass because current password is not valid", function() {
        	ds.Account.addUser(user1,true);
            ds.Account.login(user1);
            var res = ds.Account.updateUser(newCredentialsWithfalseold);
            expect(res.status).to.be.false;
            expect(res.msg).to.equal("The current password is not valid");
            expect(res.obj.role).to.be.empty;
        });
        it("should update user credentials", function() {        	
            var res = ds.Account.updateUser(newCredentials);
            expect(res.status).to.be.true;
            expect(res.msg).to.equal("update OK");
            expect(res.obj.role).to.be.empty;
        });
        after(function() {
        	coll = ds.Collectivity.query("name == :1", user1.collectivity)
            if (coll[0]) coll[0].remove();
            user = ds.Account.query("userName == :1", user1.userName)
            if(user[0]) user[0].remove();
            logout();
            res.remove;
        });
    });
    describe("test user logout", function() {
        it("should logout user", function() {
            var res = ds.Account.logout();
            expect(res).to.be.true;
        });
    });
    describe("test if email exists", function() {
        it("should return failure because email doesn't exists", function() {
            var res = ds.Account.findUserByEmail(user1.userName)
            expect(res.status).to.be.false;
            expect(res.msg).to.equal("there is no user with this email");
            expect(res.obj.role).to.be.empty;
        });
        it("should return success because email exists", function() {
            ds.Account.addUser(user1,true);
            var res = ds.Account.findUserByEmail(user1.userName);
            expect(res.status).to.be.true;
            expect(res.msg).to.equal("user mail is found");
            expect(res.obj).to.equal(user1.userName);
        });
        after(function() {
        	var coll = ds.Collectivity.query("name == :1", user1.collectivity)
            if (coll[0]) coll[0].remove();
            var user = ds.Account.query("userName == :1", user1.userName)
            if(user[0]) user[0].remove();
            ds.Account.logout();
       });
    });
    describe("test token when changing password", function() {
        it("should return failure token doesn't exists", function() {
            var res = ds.Account.findUserByToken("falseToken","newPassword")
            expect(res.status).to.be.false;
            expect(res.msg).to.equal("Ce token n'existe pas");
            expect(res.obj).to.be.empty;
        });
        it("should return success because token exists", function() {   
        	var usr = ds.Account.find("userName == :1", user1.userName);
        	usr.token_pwd = "passwordToken";
        	usr.save();
            var res = ds.Account.findUserByToken("passwordToken","newPassword")
            expect(res.status).to.be.true;
            expect(res.msg).to.equal("token found");
            expect(res.obj).to.be.empty;
        });
        beforeEach(function(){
		    ds.Account.addUser(user1,true);
		})
        afterEach(function(){
		    var user = ds.Account.query("userName == :1", user1.userName)
            if(user[0]) user[0].remove();
            var coll = ds.Collectivity.query("name == :1", user1.collectivity)
            if (coll[0]) coll[0].remove();
		});

    });
    describe("test if token is valid", function() {
    	beforeEach(function(){
		    ds.Account.addUser(user1,true);
		})
        afterEach(function(){
		    var user = ds.Account.query("userName == :1", user1.userName)
            if(user[0]) user[0].remove();
            var coll = ds.Collectivity.query("name == :1", user1.collectivity)
            if (coll[0]) coll[0].remove();
		});
        it("should return failure token doesn't exists", function() {
            var res = ds.Account.isTokenValid("falseToken")
            expect(res.status).to.be.false;
            expect(res.msg).to.equal("Ce token n'existe pas");
            expect(res.obj).to.be.empty;
        });
        it("should return failure because token date has expired", function() {   
        	var usr = ds.Account.find("userName == :1", user1.userName);
        	usr.token_pwd = "passwordToken";
       	    var yesterday = new Date();
        	yesterday.setDate(yesterday.getDate() - 1);
        	usr.token_expire_date = yesterday;
        	usr.save();
            var res = ds.Account.isTokenValid("passwordToken")
            expect(res.status).to.be.false;
            expect(res.msg).to.equal("Token date has expired");
            expect(res.obj).to.be.empty;
        });
        it("should return success because token exists and date is valid", function() {
        	var usr = ds.Account.find("userName == :1", user1.userName);
        	usr.token_pwd = "passwordToken";
       	    var tomorrow = new Date();
        	tomorrow.setDate(tomorrow.getDate() + 1);
        	usr.token_expire_date = tomorrow;
        	usr.save();
            var res = ds.Account.isTokenValid("passwordToken")
            expect(res.status).to.be.true;
            expect(res.msg).to.equal("Token is valid");
            expect(res.obj).to.be.empty;
        });

    });
    after(function() {
       currentSession().unPromote(1);	
    });
});

 
