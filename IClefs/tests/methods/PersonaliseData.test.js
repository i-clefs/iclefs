var Constantes = require("../../backend/modules/const.js");

describe("test PersonaliseData methods", function () {
    var service;
    var token;
    var data = [
        {
            name : "personaliseDataUnitTest",
            _type : "type",
            isRequired : true
        },
        {
            name : "personaliseDataUnitTest",
            _type : "type",
            isRequired : true
        }
    ];
    
    describe("addField method", function () {
        before(function () {
            token = currentSession().promoteWith(Constantes.admin);
            service = new ds.Services();
            service.ID = generateUUID();
            service.name = "ServiceUnitTest";
            service.save();
            currentSession().unPromote(token);
        });
        after(function () {
            token = currentSession().promoteWith(Constantes.admin);
            var ser = ds.Services.find("ID == :1",service.ID);
            if(ser)
                ser.remove();
            var personalisData = ds.PersonaliseData.query("name == :1","personaliseDataUnitTest");
            if(personalisData){
                personalisData.forEach(function (obj) {
                    obj.remove();
                });
            }
            currentSession().unPromote(token);
        });
        
        it("personaliseData should be create", function () {
            token = currentSession().promoteWith(Constantes.admin);
            var result = ds.PersonaliseData.addField(data,service);
            expect(result).to.be.an('object');
            expect(result.status).equal(true);
            expect(result.msg).equal("bien ajouter");
            currentSession().unPromote(token);
        });

        it("call function with invalid data", function () {
            token = currentSession().promoteWith(Constantes.admin);
            var result = ds.PersonaliseData.addField("",service);
            expect(result).to.be.an('object');
            expect(result.status).equal(false);
            expect(result.msg).equal("paramètre incorrecte");
            currentSession().unPromote(token);
        });

        it("call function with service doesn't existe", function () {
            token = currentSession().promoteWith(Constantes.admin);
            service.ID = "1";
            var ser = new ds.Services();
            ser.ID = generateUUID();
            var result = ds.Mail.addMail(data,ser);
            expect(result).to.be.an('object');
            expect(result.status).equal(false);
            expect(result.msg).equal("Service n'existe pas");
            currentSession().unPromote(token);
        });

        it("call function with empty paramaters", function () {
            token = currentSession().promoteWith(Constantes.admin);
            var result = ds.Mail.addMail([],"");
            expect(result).to.be.an('object');
            expect(result.status).equal(false);
            expect(result.msg).equal("paramètre incorrecte");
            currentSession().unPromote(token);
        });
    });

    describe("getFields method", function () {
        before(function () {
            token = currentSession().promoteWith(Constantes.admin);
            service = new ds.Services();
            service.ID = generateUUID();
            service.name = "ServiceUnitTest";
            service.save();
            var personaliseData = new ds.PersonaliseData();
            personaliseData.name = "personaliseDataUnitTest";
            personaliseData.services = service;
            personaliseData.save();
            currentSession().unPromote(token);
        });
        after(function () {
            token = currentSession().promoteWith(Constantes.admin);
            var ser = ds.Services.find("ID == :1",service.ID);
            if(ser)
                ser.remove();
            var perData = ds.PersonaliseData.query("name == :1","personaliseDataUnitTest");
            if(perData){
                perData.forEach(function (obj) {
                    obj.remove();
                });
            }
            currentSession().unPromote(token);
        });

        it("PersonaliseData of the Service should be returned", function () {
            token = currentSession().promoteWith(Constantes.admin);
            var result = ds.PersonaliseData.getFields(service.ID);
            expect(result).to.be.an('object');
            expect(result.obj).to.be.an('array');
            expect(result.status).equal(true);
            expect(result.msg).equal("ce service contient des données personnaliser");
            currentSession().unPromote(token);
        });

        it("call function with service doesn't existe", function () {
            token = currentSession().promoteWith(Constantes.admin);
            var ser = new ds.Services();
            ser.ID = generateUUID();
            var result = ds.PersonaliseData.getFields(ser.ID);
            expect(result).to.be.an('object');
            expect(result.status).equal(false);
            expect(result.msg).equal("service n'existe pas");
            currentSession().unPromote(token);
        });

        it("call function with empty paramaters", function () {
            token = currentSession().promoteWith(Constantes.admin);
            var result = ds.PersonaliseData.getFields("");
            expect(result).to.be.an('object');
            expect(result.status).equal(false);
            expect(result.msg).equal("paramètre incorrecte");
            currentSession().unPromote(token);
        });

        it("call function with service contain empty PersonaliseData", function () {
            token = currentSession().promoteWith(Constantes.admin);
            var perData = ds.PersonaliseData.query("name == :1","personaliseDataUnitTest");
            if(perData){
                perData.forEach(function (obj) {
                    obj.remove();
                });
            }
            var result = ds.PersonaliseData.getFields(service.ID);
            expect(result).to.be.an('object');
            expect(result.status).equal(false);
            expect(result.msg).equal("ce service ne contient pas des données personnaliser");
            currentSession().unPromote(token);
        });

        it("call function with service does not contain PersonaliseData", function () {
            token = currentSession().promoteWith(Constantes.admin);
            var perData = ds.PersonaliseData.query("name == :1","personaliseDataUnitTest");
            var service_2 = new ds.Services();
            service_2.ID = generateUUID();
            service_2.save();
            var result = ds.PersonaliseData.getFields(service.ID);
            expect(result).to.be.an('object');
            expect(result.status).equal(false);
            expect(result.msg).equal("ce service ne contient pas des données personnaliser");
            var ser = ds.Services.find("ID == :1",service_2.ID);
            if(ser)
                ser.remove();
            currentSession().unPromote(token);
        });
    });
});