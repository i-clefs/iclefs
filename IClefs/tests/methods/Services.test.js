var Constantes = require("../../backend/modules/const.js");

describe("test Services methods", function () {
    var service;
    var token;
    var collectivity;
    var account;
    var config = {
        identifiant : true,
        avisImposition  :   true,
        adresseFiscale : false,
        quotientFamilial : false,
        compositionFamiliale : false,
        adresse : false
    };
    var mails = [];
    var personaliseData = [];
    var data = {
        client_Id: "673f4653548476200f84db2116a752d518c01b4cd6a88a5d7161810646ef7dd3",
        client_secret: "d50d2ca457f46e8f3478c4246c6528457374ccc5cdd47b4b5717b7d0086ba015",
        name: "collectivityTesting",
        zipCode: "zipCode",
        postRedirectUri: "postRedirectUri"
    };
    var dataModel = {
        name : "serviceTesting",
        data : config,
        collectivity : data.name,
    };

    describe("addService method", function () {
        before(function(){
            token = currentSession().promoteWith(Constantes.agent);
            collectivity = new ds.Collectivity();
            collectivity.name = data.name;
            collectivity.client_Id = data.client_Id;
            collectivity.client_secret = data.client_secret;
            collectivity.save();
            account = new ds.Account();
            account.userName = "adress@mail.com";
            account.role = {tab: [Constantes.admin]};
            account.collectivity = collectivity;
            account.save();
            currentSession().unPromote(token);
        });
        after(function () {
            token = currentSession().promoteWith(Constantes.superAdmin);
            var col = ds.Collectivity.find("ID == :1", collectivity.ID);
            if(col)
                col.remove();
            var acc = ds.Account.find("ID == :1", account.ID);
            if(acc)
                acc.remove();
            currentSession().unPromote(token);
        });
        afterEach(function () {
            token = currentSession().promoteWith(Constantes.superAdmin);
            var ser = ds.Services.find("name == :1", dataModel.name);
            if(ser)
                ser.remove();
            currentSession().unPromote(token);
        });

        it("new service should be created", function () {
            token = currentSession().promoteWith(Constantes.admin);
            var result = ds.Services.addService(dataModel,personaliseData,mails);
            expect(result).to.be.an('object');
            expect(result.obj).to.be.an('object');
            expect(result.status).equal(true);
            expect(result.msg).equal("Service bien crée");
            currentSession().unPromote(token);
        });

        it("service should not be created with null config", function () {
            token = currentSession().promoteWith(Constantes.admin);
            var result = ds.Services.addService(null,personaliseData,mails);
            expect(result).to.be.an('object');
            expect(result.status).equal(false);
            expect(result.msg).equal("Invalide parameter");
            currentSession().unPromote(token);
        });

        it("service should not be created with invalid parameters", function () {
            token = currentSession().promoteWith(Constantes.admin);
            var result = ds.Services.addService("","","");
            expect(result).to.be.an('object');
            expect(result.status).equal(false);
            expect(result.msg).equal("Une erreur est survenue");
            currentSession().unPromote(token);
        });

        it("service should not be created without admin of collectivity", function () {
            token = currentSession().promoteWith(Constantes.admin);
            var acc = ds.Account.find("ID == :1", account.ID);
            if(acc)
                acc.remove();
            var result = ds.Services.addService(dataModel,personaliseData,mails);
            expect(result).to.be.an('object');
            expect(result.status).equal(false);
            expect(result.msg).equal("admin de cette collectivité n'existe pas");
            currentSession().unPromote(token);
        });

    });

    describe("testIdSecret method", function () {
        before(function () {
            collectivity = new ds.Collectivity();
            collectivity.name = data.name;
            collectivity.client_Id = data.client_Id;
            collectivity.client_secret = data.client_secret;
            collectivity.save();

        });
        after(function () {
            token = currentSession().promoteWith(Constantes.superAdmin);
            var col = ds.Collectivity.find("ID == :1", collectivity.ID);
            if(col)
                col.remove();
            currentSession().unPromote(token);
        });

        it("call function with valide client_secret and client_Id of collectivity", function () {
            token = currentSession().promoteWith(Constantes.agent);
            var result = ds.Services.testIdSecret(collectivity.name);
            expect(result).to.be.an('object');
            expect(result.status).equal(true);
            expect(result.msg).equal("(clef et secret) client existe");
            currentSession().unPromote(token);
        });

        it("call function with empty parameter", function(){
            token = currentSession().promoteWith(Constantes.agent);
            var result = ds.Services.testIdSecret(null);
            expect(result).to.be.an('object');
            expect(result.status).equal(false);
            expect(result.msg).equal("paramètre erroné");
            currentSession().unPromote(token);
        });

        it("call function with collectivity doesn't existe", function () {
            token = currentSession().promoteWith(Constantes.agent);
            var result = ds.Services.testIdSecret("0012");
            expect(result).to.be.an('object');
            expect(result.status).equal(false);
            expect(result.msg).equal("collectivité n'existe pas");
            currentSession().unPromote(token);
        });

        it("call function with empty client_secret and client_Id of collectivity", function () {
            token = currentSession().promoteWith(Constantes.admin);
            var col = ds.Collectivity.find("name == :1",data.name);
            if(col){
                col.client_Id = null;
                col.client_secret = null;
                col.save();
            }
            var result = ds.Services.testIdSecret(col.name);
            expect(result).to.be.an('object');
            expect(result.status).equal(false);
            expect(result.msg).equal("(clef et secret) client n'existe pas");
            currentSession().unPromote(token);
        });
    });

    describe("findServiceByID method", function () {
        before(function () {
            token = currentSession().promoteWith(Constantes.admin);
            collectivity = new ds.Collectivity();
            collectivity.name = data.name;
            collectivity.postRedirectUri = data.postRedirectUri;
            collectivity.save();
            service = new ds.Services();
            service.ID = generateUUID();
            service.name = "ServiceUnitTest";
            service.config = config;
            service.collectivity = ds.Collectivity.find("name == :1", collectivity.name);
            service.save();
            currentSession().unPromote(token);
        });
        after(function () {
            token = currentSession().promoteWith(Constantes.superAdmin);
            var col = ds.Collectivity.find("ID == :1", collectivity.ID);
            if(col)
                col.remove();
            var ser = ds.Services.find("ID == :1", service.ID);
            if(ser)
                ser.remove();
            currentSession().unPromote(token);
        });
        
        it("get service with pageRedirect", function () {
            var result = ds.Services.findServiceByID(service.ID,"dgfip");
            expect(result).to.be.an('object');
            expect(result.obj).to.be.an('object');
            expect(result.obj.page).to.be.an('string');
            expect(result.status).equal(true);
            expect(result.msg).equal("Service existe");
        });

        it("call function with empty parameter", function () {
            var result = ds.Services.findServiceByID(null,"");
            expect(result).to.be.an('object');
            expect(result.status).equal(false);
            expect(result.msg).equal("paramètre erroné");
        });

        it("call function with service doesn't existe", function () {
            var result = ds.Services.findServiceByID(1010100,"");
            expect(result).to.be.an('object');
            expect(result.status).equal(false);
            expect(result.msg).equal("service n'existe pas");
        });

        it("call function without defining source", function () {
            var result = ds.Services.findServiceByID(service.ID,"");
            expect(result).to.be.an('object');
            expect(result.obj).to.be.an('object');
            expect(result.obj.page).equal(null);
            expect(result.status).equal(true);
            expect(result.msg).equal("Service existe");
        });

        it("call function with endRequest source", function () {
            var result = ds.Services.findServiceByID(service.ID,"endRequest");
            expect(result).to.be.an('object');
            expect(result.obj).to.be.an('object');
            expect(result.obj.page).equal(null);
            expect(result.obj.postRedirectUri).to.be.an('string');
            expect(result.status).equal(true);
            expect(result.msg).equal("Service existe");
        });

        it("call function with endRequest source without postredirect of collectivity", function () {
            token = currentSession().promoteWith(Constantes.admin);
            var col = ds.Collectivity.find("ID == :1", collectivity.ID);
            if(col){
                collectivity.postRedirectUri = null;
                collectivity.save();
            }
            currentSession().unPromote(token);
            var result = ds.Services.findServiceByID(service.ID,"endRequest");
            expect(result).to.be.an('object');
            expect(result.obj).to.be.an('object');
            expect(result.obj.page).equal(null);
            expect(result.obj.postRedirectUri).equal(null);
            expect(result.status).equal(true);
            expect(result.msg).equal("Service existe");
        });

    });
});