cst = require('const');
var ticket , agentToken;
describe("Test Ticket methods", function() {
	this.timeout(8000);
	before(function() {
		var token = currentSession().promoteWith(cst.superAdmin);
		ticket = ds.Ticket.createEntity();
		ticket.fcData = {};
		ticket.status = false;
		ticket.save();
        currentSession().unPromote(token);	
	});
	describe("test validate and refuse Tickets", function() {
		it("should not be able to validate a ticket because current user is not agent", function() {
			try{
         	   res = ticket.Validate();
            }
            catch (e){
            	expect(e.error[0].errCode).to.equal(21);
            	expect(e.error[0].message).to.equal("Current session failed permission for group Agent")
            }
        });
        it("should not be able to refuse a ticket because current user is not agent", function() {
			try{
            	res = ticket.refused();
            }
            catch (e){
            	expect(e.error[0].errCode).to.equal(21);
            	expect(e.error[0].message).to.equal("Current session failed permission for group Agent")
            }
        });
        it("should not be able to validate a ticket because it doesn't belong to any collectivity", function() {
			agentToken = currentSession().promoteWith(cst.agent);
         	res = ticket.Validate();
         	expect(res.status).to.be.false;
         	expect(res.msg).to.equal("ce ticket n'appartient à aucune collectivité");
         	expect(res.obj).to.be.empty;

        });
        it("should not be able to refuse a ticket a ticket because it doesn't belong to any collectivity", function() {
         	res = ticket.refused();
         	expect(res.status).to.be.false;
         	expect(res.msg).to.equal("ce ticket n'appartient à aucune collectivité");
         	expect(res.obj).to.be.empty;
        });
        it("should not be able to validate a ticket because it doesn't belong to any service", function() {
        	coll = ds.Collectivity.createEntity();
        	coll.save();
        	ticket.collectivity = coll;
        	ticket.save()
         	res = ticket.Validate();
         	expect(res.status).to.be.false;
         	expect(res.msg).to.equal("ce ticket n'appartient à aucun service");
         	expect(res.obj).to.be.empty;

        });
        it("should not be able to refuse a ticket a ticket because it doesn't belong to any service", function() {
         	res = ticket.refused();
         	expect(res.status).to.be.false;
         	expect(res.msg).to.equal("ce ticket n'appartient à aucun service");
         	expect(res.obj).to.be.empty;
        });
        it("should be able to validate a ticket", function() {
			var token = currentSession().promoteWith(cst.superAdmin);
        	service = ds.Services.createEntity(); 
        	service.ID = generateUUID();
        	service.save();
        	currentSession().unPromote(token);
        	ticket.service = service;
        	ticket.save()
         	res = ticket.Validate();
         	expect(res.status).to.be.true;
         	expect(res.msg).to.equal("Validate OK");
         	expect(res.obj).to.be.an('object');;

        });
        it("should  be able to refuse a ticket", function() {
         	res = ticket.refused();
         	expect(res.status).to.be.true;
         	expect(res.msg).to.equal("Refused OK");
         	expect(res.obj).to.be.an('object');
         	currentSession().unPromote(agentToken);

        });
	}),
	describe("test get ticket by ID", function() {
		it("should not be able to get ticket because no ID is sent", function() {
         	res = ds.Ticket.findTicketByID();
         	expect(res.status).to.be.false;
         	expect(res.msg).to.equal("aucun ID n'est envoyé");
         	expect(res.obj).to.be.empty;   
        });
        it("should not be able to get ticket because no ticket exist with this ID", function() {
         	res = ds.Ticket.findTicketByID("testFalseID");
         	expect(res.status).to.be.false;
         	expect(res.msg).to.equal("Ticket n'existe pas");
         	expect(res.obj).to.be.empty;   
        });
        it("should be able to get ticket", function() {
         	res = ds.Ticket.findTicketByID(ticket.ID);
         	expect(res.status).to.be.true;
         	expect(res.msg).to.equal("ticket found");
         	expect(res.obj).to.be.an('object');  
        });
    }),
	after(function() {
		var token = currentSession().promoteWith(cst.superAdmin);
		var t = ds.Ticket.find("ID ==:1",ticket.ID)
		if(t) t.remove();
		var c = ds.Collectivity.find("ID ==:1",coll.ID)
		if(c) c.remove();
		var s = ds.Services.find("ID ==:1",service.ID)
		if(s) s.remove();
    	currentSession().unPromote(token);	
    });
})
