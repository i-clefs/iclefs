var Constantes = require("../../backend/modules/const.js");

describe("test Mail methods", function () {
    var service;
    var mail;
    var token;
    var mails = [
        {
            _type : "mailUnitTest",
            subject : "subject",
            body : "body",
        },
        {
            _type : "mailUnitTest",
            subject : "subject",
            body : "body",
        },
        {
            _type : "mailUnitTest",
            subject : "subject",
            body : "body",
        },
    ];

    describe("addMail method", function () {
       before(function () {
           token = currentSession().promoteWith(Constantes.admin);
           service = new ds.Services();
           service.ID = generateUUID();
           service.name = "ServiceUnitTest";
           service.save();
           currentSession().unPromote(token);
       });
       after(function () {
           token = currentSession().promoteWith(Constantes.admin);
           var ser = ds.Services.find("ID == :1",service.ID);
           if(ser)
               ser.remove();
           var mails_collection = ds.Mail.query("type == :1","mailUnitTest");
           if(mails_collection){
               mails_collection.forEach(function (mail) {
                  mail.remove();
               });
           }
           currentSession().unPromote(token);
       });
       
       it("mails should be create", function () {
           token = currentSession().promoteWith(Constantes.admin);
           var result = ds.Mail.addMail(mails,service);
           expect(result).to.be.an('object');
           expect(result.status).equal(true);
           expect(result.msg).equal("mails bien crée");
           currentSession().unPromote(token);
       });

       it("call function with invalid mails", function () {
            token = currentSession().promoteWith(Constantes.admin);
            var result = ds.Mail.addMail("UnitTest",service);
            expect(result).to.be.an('object');
            expect(result.status).equal(false);
            expect(result.msg).equal("paramètre incorrecte");
            currentSession().unPromote(token);
        });

       it("call function with service doesn't existe", function () {
           token = currentSession().promoteWith(Constantes.admin);
           service.ID = "1";
           var result = ds.Mail.addMail(mails,service);
           expect(result).to.be.an('object');
           expect(result.status).equal(false);
           expect(result.msg).equal("Service n'existe pas");
           currentSession().unPromote(token);
       });

       it("call function with empty paramaters", function () {
           token = currentSession().promoteWith(Constantes.admin);
           var result = ds.Mail.addMail([],"");
           expect(result).to.be.an('object');
           expect(result.status).equal(false);
           expect(result.msg).equal("paramètre incorrecte");
           currentSession().unPromote(token);
       });

    });

    describe("updateMail method", function () {
       before(function () {
           token = currentSession().promoteWith(Constantes.admin);
           service = new ds.Services();
           service.ID = generateUUID();
           service.name = "ServiceUnitTest";
           service.save();
           mail = new ds.Mail();
           mail.service = service;
           mail.save();
           currentSession().unPromote(token);
       });
       after(function () {
          token = currentSession().promoteWith(Constantes.admin);
          var ser = ds.Services.find("name == :1",service.name);
          if(ser)
              ser.remove();
          var ml = ds.Mail.find("ID == :1",mail.ID);
          if(ml)
              ml.remove();
          currentSession().unPromote(token);
       });

       it("mail should be updated", function () {
           token = currentSession().promoteWith(Constantes.admin);
           mail.type = "type";
           mail.subject = "subject";
           mail.body = "body";
           var result = ds.Mail.updateMail(mail,service.ID);
           console.log(result);
           expect(result).to.be.an('object');
           expect(result.status).equal(true);
           expect(result.msg).equal("la mise à jour est faite");
           currentSession().unPromote(token);
       });

       it("mail should not be updated without service", function () {
           token = currentSession().promoteWith(Constantes.admin);
           mail.type = "type";
           mail.subject = "subject";
           mail.body = "body";
           var result = ds.Mail.updateMail(mail,"1");
           expect(result).to.be.an('object');
           expect(result.status).equal(false);
           expect(result.msg).equal("service n'existe pas");
           currentSession().unPromote(token);
       });

       it("mail should not be updated without empty fields", function () {
           token = currentSession().promoteWith(Constantes.admin);
           mail.type = null;
           mail.subject = null;
           mail.body = null;
           var result = ds.Mail.updateMail(mail,service.ID);
           expect(result).to.be.an('object');
           expect(result.status).equal(false);
           expect(result.msg).equal("champs vides");
           currentSession().unPromote(token);
       });

       it("mail should not be updated with invalid parameters", function () {
           token = currentSession().promoteWith(Constantes.admin);

           var result = ds.Mail.updateMail("",service.ID);
           expect(result).to.be.an('object');
           expect(result.status).equal(false);
           expect(result.msg).equal("paramètre incorrecte");
           currentSession().unPromote(token);
       });
    });
});