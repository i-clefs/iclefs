var Constantes = require("../../backend/modules/const.js");

describe("test Collectivity methods", function () {
	this.timeout(8000);
    var collectivity;
    var account;
    var data = {
        client_Id: "client_Id",
        client_secret: "client_secret",
        name: "name",
        zipCode: "zipCode",
        postRedirectUri: "postRedirectUri"
    };

    describe("setClient method", function () {
		var token;
        beforeEach(function () {
            token = currentSession().promoteWith(Constantes.admin);
            collectivity = new ds.Collectivity();
        });

        afterEach(function () {
            currentSession().unPromote(token);
            token = currentSession().promoteWith(Constantes.superAdmin);
            var col = ds.Collectivity.find("ID == :1", collectivity.ID);
            if(col) col.remove();
            currentSession().unPromote(token);
        });

        it("the collectivity should be updated", function () {
            var result = collectivity.setClient(data);
            expect(result).to.be.an('object');
            expect(result.status).equal(true);
            expect(result.msg).equal("la mise à jour est faite");
        });

        it("call function with null parameter", function () {
            var result = collectivity.setClient(null);
            expect(result).to.be.an('object');
            expect(result.status).equal(false);
            expect(result.msg).equal("paramètre erroné");
        });
    });

    describe("valideRefuse method", function () {


        beforeEach(function () {
            var token = currentSession().promoteWith(Constantes.agent);
            try{
	            collectivity = new ds.Collectivity();
	            account = new ds.Account();
	            collectivity.name = data.name;
	            collectivity.active = false;
	            collectivity.save();
	            account.userName = "adress@mail.com";
	            account.collectivity = collectivity;
	            account.save();
            }
            catch(error){
	            currentSession().unPromote(token);
            }
            finally{
  	            currentSession().unPromote(token);
            }
        });

        afterEach(function () {
            var token = currentSession().promoteWith(Constantes.superAdmin);
            try{
				var col = ds.Collectivity.find("ID == :1", collectivity.ID);
	            if(col) col.remove();
	            var acc = ds.Account.find("ID == :1", account.ID);
	            if(acc) acc.remove();
	        }
            catch(error){
	            currentSession().unPromote(token);
            }
            finally{
  	            currentSession().unPromote(token);
            }            
        });

        it("validate request should pass", function () {
            var token = currentSession().promoteWith(Constantes.superAdmin);
            try{
				var result = ds.Collectivity.valideRefuse(account.ID,collectivity.ID,"valide");
	            expect(result).to.be.an('object');
	            expect(result.status).equal(true);
	            expect(result.msg).equal("la demande est validée");
	        }
            catch(error){
	            currentSession().unPromote(token);
            }
            finally{
  	            currentSession().unPromote(token);
            }    
        });

        it("refuse request should pass", function () {
            try{
	            var token = currentSession().promoteWith(Constantes.superAdmin);
	            var result = ds.Collectivity.valideRefuse(account.ID,collectivity.ID,"refuse");
	            expect(result).to.be.an('object');
	            expect(result.status).equal(true);
	            expect(result.msg).equal("la demande est refusé");
	            var token = currentSession().promoteWith(Constantes.superAdmin);
	        }
            catch(error){
	            currentSession().unPromote(token);
            }
  	        currentSession().unPromote(token);
        });

        it("call function with null account and collectivity", function () {
            var token = currentSession().promoteWith(Constantes.superAdmin);
            try{
	            var result = ds.Collectivity.valideRefuse(null,null,"");
	            expect(result).to.be.an('object');
	            expect(result.status).equal(false);
	            expect(result.msg).equal("paramètre erroné");
            }
            catch(error){
	            currentSession().unPromote(token);
            }
  	         currentSession().unPromote(token);
        });

        it("call function with accountID and collectivityID doesn't existe", function () {
            var token = currentSession().promoteWith(Constantes.superAdmin);            
            try{
	            var result = ds.Collectivity.valideRefuse(1010,1010,"");
	            expect(result).to.be.an('object');
	            expect(result.status).equal(false);
	            expect(result.msg).equal("compte n'existe pas");
	     	}
            catch(error){
	            currentSession().unPromote(token);
            }
  	        currentSession().unPromote(token);
            
        });

        it("call function without define a operator", function () {
            var token = currentSession().promoteWith(Constantes.superAdmin);
        	try{
	            var result = ds.Collectivity.valideRefuse(account.ID,collectivity.ID,"");
	            expect(result).to.be.an('object');
	            expect(result.status).equal(false);
	     	}
            catch(error){
	            currentSession().unPromote(token);
            }
  	        currentSession().unPromote(token);
            
        });
    });
});