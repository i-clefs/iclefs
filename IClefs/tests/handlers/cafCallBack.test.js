var constatnte = require("../../backend/modules/const.js");
//define variable with parameters returned by Fc
var fcData = {
    address: {
        country: "France",
        formatted: "26 rue Desaix, 75015 Paris",
        locality: "Paris",
        postal_code: "75015",
        region: "Ile-de-France",
        street_address: "26 rue Desaix"
    },
    birthcountry: "FRANCE",
    birthdate: "1981-06-23",
    birthplace: "Gif-sur-Yvette",
    email: "eric.mercier@france.fr",
    family_name: "Mercier",
    gender: "male",
    given_name: "Eric",
    sub: "3e7b92b9a94d96c6f3517aef4dfa1ef07c02f0a7e715c30c0fd72feae615b64dv1"
};
var FCdata = {
    key : "fcData",
    value : fcData
};
//define variable with parameters returned by apiParticulier DGFIP
var apiData = {
    declarant1: {
        nom: "Martin",
        nomNaissance: "Martin",
        prenoms: "Pierre",
        dateNaissance: "22/03/1985"
    },
    declarant2: {
        nom: "Martin",
        nomNaissance: "Honore",
        prenoms: "Marie",
        dateNaissance: "03/04/1986"
    },
    foyerFiscal: {
        annee: 2015,
        adresse: "12 rue Balzac 75008 Paris"
    },
    dateRecouvrement: "10/10/2015",
    dateEtablissement: "08/07/2015",
    nombreParts: 2,
    situationFamille: "Marié(e)s",
    nombrePersonnesCharge: 2,
    revenuBrutGlobal: 29880,
    revenuImposable: 29880,
    impotRevenuNetAvantCorrections: 2165,
    montantImpot: 2165,
    revenuFiscalReference: 29880,
    anneeImpots: "2015",
    anneeRevenus: "2014"
};
//set data for xhr parameters
var data = {
    codePostal: 99148,
    numeroAllocataire: "0000354",
    refService: "20002ABB63E1CA468A568CB461A4CAC7" //service with config fc and caf
};
var invalidData = {
    codePostal: 45894798,
    numeroAllocataire: "2584984",
    refService: "4984984984" 
};
var url = "/api/v1/caf/callback";
var token;
var ticketID = 0;

describe("testing cafCallBack", function () {
    var result;

    describe("test the session Storage", function () {
        var result;
        before(function() {
            this.timeout(5000);
            var FCdata = {
                key : "fcData",
                value : fcData
            };
            var xhr_1 = new XMLHttpRequest();
            xhr_1.open('POST', constatnte.uri+"/setSessionStorage");
            xhr_1.setRequestHeader("Content-type","application/json;charset=UTF-8");
            xhr_1.onreadystatechange = function () {
                if(xhr_1.readyState === 4 && xhr_1.status === 200){
                    var res = JSON.parse(xhr_1.response);
                }
            };
            xhr_1.send(JSON.stringify(FCdata));
        });
        beforeEach(function () {
            this.timeout(5000);
            var xhr_2 = new XMLHttpRequest();
            xhr_2.open('POST', constatnte.uri+url);
            xhr_2.setRequestHeader("Content-type","application/json;charset=UTF-8");
            xhr_2.onreadystatechange = function () {
                if(xhr_2.readyState === 4 && xhr_2.status === 200){
                    result = JSON.parse(xhr_2.response);
                    ticketID = result.ticketID;
                }
            }
            xhr_2.send(JSON.stringify(data));
        });
        afterEach(function () {
            token = currentSession().promoteWith(Constantes.agent);
            if(ticketID == 0){
                var ticket = ds.Ticket.find("ID == :1",ticketID);
                if(ticket) ticket.remove();
            }
            currentSession().unPromote(token);
        });

        it("test with correct data and sessionStorage already exists", function () {
            expect(result).to.be.an('object');
            expect(result.messageText).equal("ticket saved");
        });

        it("test with correct data and empty sessionStorage", function () {
            expect(result).to.be.an('object');
            expect(result.messageText).equal("identifiant pivote n'est pas reçu");
        });
    });

    describe("test particulier (CAF)", function () {
        var result2;
        before(function () {
            data.codePostal = "0000354";
            data.numeroAllocataire = 99148;
            data.refService = "0101";
        });
        beforeEach(function () {
            this.timeout(5000);

            var xhr_1 = new XMLHttpRequest();
            xhr_1.open('POST', constatnte.uri+"/setSessionStorage");
            xhr_1.setRequestHeader("Content-type","application/json;charset=UTF-8");
            xhr_1.onreadystatechange = function () {
                if(xhr_1.readyState === 4 && xhr_1.status === 200){
                    var res = JSON.parse(xhr_1.response);
                }
            };
            xhr_1.send(JSON.stringify(FCdata));

            var xhr_2 = new XMLHttpRequest();
            xhr_2.open('POST', constatnte.uri+url);
            xhr_2.setRequestHeader("Content-type","application/json;charset=UTF-8");
            xhr_2.onreadystatechange = function () {
                if(xhr_2.readyState === 4 && xhr_2.status === 200){
                    result2 = JSON.parse(xhr_2.response);
                    ticketID = result2.ticketID;
                }
            }
            xhr_2.send(JSON.stringify(data));

            data.codePostal = 99148;
            data.numeroAllocataire = "0000354";
        });
        
        afterEach(function () {
            token = currentSession().promoteWith(Constantes.agent);
            if(ticketID == 0){
                var ticket = ds.Ticket.find("ID == :1",ticketID);
                if(ticket) ticket.remove();
            }
            currentSession().unPromote(token);
        });
        it("ticket should not be created with invalid identifier", function () {
            expect(result2).to.be.an('object');
            expect(result2.messageText).equal("Dossier allocataire inexistant. Le document ne peut être édité.");
        });

        it("ticket should not be created with service does not exist", function () {
            expect(result2).to.be.an('object');
            expect(result2.messageText).equal("service n'existe pas");
        });


    });

});