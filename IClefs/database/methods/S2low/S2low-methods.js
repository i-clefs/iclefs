

model.S2low.entityMethods.savefunc = function() {
	  function mailSend ()
            {
                var xhr = new XMLHttpRequest();

                xhr.open("POST", "XXXXXXXX/modules/mail/api/send-mail.php", true);
                // MANQUE LES PARAMETRES
                xhr.onreadystatechange = function(event)
                {
                    if (xhr.readyState == 4 && xhr.status == 200)
                    {
                        if (reponse == null)
                        {
                            console.log("reponse == NULL");
                        }
                        else
                        {
                            var reponseValue = xhr.reponseText; // ---------------------------------------> attention à changer peut-etre
                            var reponseTab = reponseValue.split(":");
                            if (reponseTab[0] == "erreur")
                            {
                                console.log("erreur : " + reponseTab[1]);
                            }
                            else
                            {
                                console.log("ok : id_mail = " + reponseTab[1]);
                            }
                        }
                    }
                }
                xhr.send(null);
            }

            function mailVersion()
            {
                var xhr = new XMLHttpRequest();

                xhr.open("GET", "XXXXXXXX/modules/mail/api/version.php", true);
                xhr.onreadystatechange = function(event)
                {
                    if (xhr.readyState == 4 && xhr.status == 200)
                    {
                        if (xhr.reponseText == null)
                        {
                            if (reponse == null)
                            {
                                console.log("reponse == NULL");
                            }
                        }
                        else
                        {
                            var reponseValue = xhr.reponseText; // ---------------------------------------> attention à changer peut-etre
                            var reponseTab = reponseValue.split("\n");
                            console.log("version API : " + reponseTab[0]);
                            console.log("details : " + reponseTab[1]);
                        }
                    }
                }
                xhr.send(null);
            }

            function mailListUsers()
            {
                var xhr = new XMLHttpRequest();

                xhr.open("GET", "XXXXXXXX/modules/mail/api/user-mail.php", true);
                xhr.onreadystatechange = function(event)
                {
                    if (xhr.readyState == 4 && xhr.status == 200)
                    {
                        if (xhr.reponseText == null)
                        {
                            if (reponse == null)
                            {
                                console.log("reponse == NULL");
                            }
                        }
                        else
                        {
                            var reponseValue = xhr.reponseText; // ---------------------------------------> attention à changer peut-etre
                            var reponseTab = reponseValue.split("\n"); // tous les users sont stocker dans reponseTab[x] apres utilisations je ferais un tableau d'objet pour les details etc
                            var i = 0;
                            console.log("List :\n");
                            while (i < reponseTab.length)
                            {
                                console.log(reponseTab[i]);
                                console.log("\n");
                                i++;
                            }
                        }
                    }
                }
                xhr.send(null);
            }

           function mailNumber()
            {
                var xhr = new XMLHttpRequest();

                xhr.open("GET", "XXXXXXXX/modules/mail/api/nb-mail.php", true);
                xhr.onreadystatechange = function(event)
                {
                    if (xhr.readyState == 4 && xhr.status == 200)
                    {
                        if (xhr.reponseText == null)
                        {
                            if (reponse == null)
                            {
                                console.log("reponse == NULL");
                            }
                        }
                        else
                        {
                            var reponseValue = xhr.reponseText; // ---------------------------------------> attention à changer peut-etre
                            console.log("Value : " + reponseValue);
                        }
                    }
                }
                xhr.send(null);
            }

            function mailList()
            {
                var xhr = new XMLHttpRequest();

                xhr.open("GET", "XXXXXXXX/modules/mail/api/list-mail.php", true);
                //ajouter les params
                xhr.onreadystatechange = function(event)
                {
                    if (xhr.readyState == 4 && xhr.status == 200)
                    {
                        if (xhr.reponseText == null)
                        {
                            if (reponse == null)
                            {
                                console.log("reponse == NULL");
                            }
                        }
                        else
                        {
                            var reponseValue = xhr.reponseText; // ---------------------------------------> attention à changer peut-etre
                            var reponseTab = reponseValue.split("\n"); // tous les mails sont stocker dans reponseTab[x] apres utilisations je ferais un tableau d'objet pour les details etc
                            var i = 0;
                            console.log("List :\n");
                            while (i < reponseTab.length)
                            {
                                console.log(reponseTab[i]);
                                console.log("\n");
                                i++;
                            }
                        }
                    }
                }
                xhr.send(null);
            }

            function mailDetails()
            {
                var xhr = new XMLHttpRequest();

                xhr.open("GET", "XXXXXXXX/modules/mail/api/detail-mail.php", true);
                //ajouter le params id mail
                xhr.onreadystatechange = function(event)
                {
                    if (xhr.readyState == 4 && xhr.status == 200)
                    {
                        if (xhr.reponseText == null)
                        {
                            if (reponse == null)
                            {
                                console.log("reponse == NULL");
                            }
                        }
                        else
                        {
                            var reponseValue = xhr.reponseText; // ---------------------------------------> attention à changer peut-etre
                            var reponseTab = reponseValue.split("\n"); // tous les details sont stocker dans reponseTab[x] apres utilisations je ferais un tableau d'objet pour les details etc
                            var i = 0;
                            console.log("Details :\n");
                            while (i < reponseTab.length)
                            {
                                console.log(reponseTab[i]);
                                console.log("\n");
                                i++;
                            }
                        }
                    }
                }
                xhr.send(null);
            }

            function mailDelete()
            {
            	var xhr = new XMLHttpRequest();

                xhr.open("POST", "XXXXXXXX/modules/mail/api/delete-mail.php", true);
                xhr.onreadystatechange = function(event)
                {
                    if (xhr.readyState == 4 && xhr.status == 200)
                    {
                        if (reponse == null)
                        {
                            console.log("reponse == NULL");
                        }
                        else
                        {
                            var reponseValue = xhr.reponseText; // ---------------------------------------> attention à changer peut-etre
                            var reponseTab = reponseValue.split(":");
                            if (reponseTab[0] == "erreur")
                            {
                                console.log("erreur : " + reponseTab[1]);
                            }
                            else
                            {
                                console.log(reponseTab[0] + " suppression");
                            }
                        }
                    }
                }
                xhr.send(null);
            }
};
