var fcTools = require("../../../backend/modules/oauth2-provider-FranceConnect/index");
var mailService = require("../../../backend/modules/emails/service/service.addService.js");
var Constantes = require("../../../backend/modules/const.js");

function constructTabFc(data) {
	var tab = [];
	tab.push(data.identifiant == true ? "Identifiant" : "none");
	tab.push(data.avisImposition == true ? "Avis d'imposition" : "none");
	tab.push(data.adresseFiscale == true ? "Adresse fiscale" : "none");
	tab.push(data.quotientFamilial == true ? "Quotient familial" : "none");
	tab.push(data.compositionFamiliale == true ? "Composition familiale" : "none");
	tab.push(data.adresse == true ? "Adresse" : "none");
	return tab;
}

//Avis d\'imposition,Adresse fiscale,Quotient familial,Composition familiale,Adresse
model.Services.methods.addService = function(dataModel,personaliseData,mails) {
		var res = {
		"status" : false,
		"msg" : "",
		"obj" : {}
	};

	if (dataModel === null || dataModel.name === "" || dataModel.name === null || personaliseData === null || mails === null) {
		res.msg = "Invalide parameter";
		return res;
	}
    var newService;
	try {
		newService = new ds.Services();
		newService.ID = generateUUID();
		newService.name = dataModel.name;
		newService.creationDate = new Date();
		newService.config = dataModel.data;
		newService.data = {tab : constructTabFc(dataModel.data)};
		newService.collectivity = ds.Collectivity.find("name == :1", dataModel.collectivity);
		var ret = fcTools.getAuthorizedUrl(newService.collectivity.client_Id, newService.collectivity.client_secret, newService.ID);
		newService.urlFc = ret.obj.data;
		newService.codeBoutton = ret.obj.request;
		newService.stat = {};
		newService.status = false;
		newService.save();
		//ajoute des donnees personaliser
		var result = ds.PersonaliseData.addField(personaliseData,newService);
		//ajoute des mail personaliser
		if(mails.length > 0){
			var resMail = ds.Mail.addMail(mails,newService);
		}
		res.obj = newService;
		var account =  getCollectivityAdmin(newService.collectivity.accountCollection);
		if(!account){
            var ser = ds.Services.find("ID == :1",newService.ID);
            if(ser)
                ser.remove();
		    res.msg = "admin de cette collectivité n'existe pas";
		    return res;
        }
		var r = mailService.sendEmail(account, newService.name, newService.collectivity.name);
        res.status = true;
        res.msg = "Service bien crée";
	}
	catch (e) {
	    var ser = ds.Services.find("ID == :1",newService.ID);
        if(ser)
            ser.remove();
		res.msg = "Une erreur est survenue";
		res.status = false;
	}
	finally {
		return res;
	}
};

getCollectivityAdmin = function(accountCollection){
	for(var i=0;i<accountCollection.length;i++ ){
		if (accountCollection[i].role.tab[i] === Constantes.admin){
			return accountCollection[i];
		}
	}
}

model.Services.methods.testIdSecret = function(nomCol){
	var res = {
		status : false,
		msg : "(clef et secret) client n'existe pas"
	};
	if(nomCol === "" || nomCol === null){
	    res.msg = "paramètre erroné";
	    return res;
    }
	var collectivity = ds.Collectivity.find("name == :1", nomCol);
	if(!collectivity){
	    res.msg = "collectivité n'existe pas";
	    return res;
    }
	if(collectivity.client_Id && collectivity.client_secret){
		res.status = true;
		res.msg = "(clef et secret) client existe";
	}
	return res;
}

model.Services.methods.findServiceByID = function(ID,source) {
    var res = {
        "status" : false,
        "msg" : "",
        "obj" : {}
    };
    var token = currentSession().promoteWith(Constantes.agent);
    var service = null;
    if(ID === null || ID === ""){
        res.msg = "paramètre erroné";
        return res;
    }

    service = ds.Services.find("ID == :1",ID);
    if(!service){
        res.msg = "service n'existe pas";
        return res;
    }
    res.obj.service = service;
    res.obj.page = null;
    res.status = true;
    res.msg = "Service existe";
    if(source === "dgfip"){
        res.status = true;
        if((service.config.adresse) || (service.config.quotientFamilial) || (service.config.compositionFamiliale))
            res.obj.page = "caf";
        else if(service.personaliseData.length > 0)
            res.obj.page = "personalisedData";
        else
            res.obj.page = "endRequest";
    }
    else if(source === "caf"){
        if(service.personaliseData.length > 0)
            res.obj.page = "personalisedData";
        else
            res.obj.page = "endRequest";
    }
    if(source === "endRequest"){
        if(service.collectivity.postRedirectUri)
            res.obj.postRedirectUri = service.collectivity.postRedirectUri;
        else
            res.obj.postRedirectUri = null;
    }
    res = JSON.parse(JSON.stringify(res));
    currentSession().unPromote(token);
    return res;
};


model.Services.methods.testIdSecret.scope = "public";
model.Services.methods.addService.scope = "public";
model.Services.methods.findServiceByID.scope = "public";




