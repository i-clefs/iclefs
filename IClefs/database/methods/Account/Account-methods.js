var requestPassword = require("../../../backend/modules/emails/password/request/password.request.js");
var changePassword = require("../../../backend/modules/emails/password/changePassword/password.changePassword.js");
var collectivityRequest = require("../../../backend/modules/emails/collectivity/request/collectivity.request.js");
var superAdminMail = require("../../../backend/modules/emails/superAdmin/superAdmin.newRequest.js");
var Constantes = require("../../../backend/modules/const.js");


model.Account.methods.addUser = function (user, superUser) {
    var res = {
        "status": false,
        "msg": "",
        "obj": {}
    };

    if (ds.Account.find("userName = :1", user.userName)) {
        res.msg = "Cette adresse email est déjà associée à un compte I-Clefs.";
        return res;
    }
    if (!user.emp) {
        if (ds.Collectivity.find("name == :1", user.collectivity)) {
            res.msg = "Cette Collectivité existe déjà sur I-Clefs.";
            return res;
        }
        else {
            var newCollectivity = new ds.Collectivity();
            newCollectivity.name = user.collectivity;
            if(user.zipcode.length !== 5){
                res.msg = "Le code postal doit contenir 5 numéros";
                res.status = false;
                return res;
            }
            newCollectivity.zipCode = user.zipcode;
            newCollectivity.CNIL = user.cnil;
            newCollectivity.active = false;
            newCollectivity.save();
        }
    }
    try {
        var newUser = new ds.Account();
        var keyHa = randomString();
        newUser.userName = user.userName;
        //newUser._password = user.password;
        newUser._password = directory.computeHA1(keyHa, user.password);
        newUser.keyHa = keyHa;
        newUser.subDate = new Date();
        newUser.firstName = user.firstName;
        newUser.lastName = user.lastName;
        if (superUser) {
            newUser.role = {tab: [Constantes.admin]};
        }
        else {
            newUser.role = {tab: [Constantes.agent]};
        }
        newUser.status = true;
        newUser.collectivity = ds.Collectivity.find("name == :1", user.collectivity);
        newUser.save();
        res.status = true;
        res.msg = "userAdd OK";
        res.obj = newUser;
        if (superUser) {
            var superAdmin = ds.Account.query("role.tab[0] == :1", Constantes.superAdmin)[0];
            if(superAdmin)
                var r2 = superAdminMail.sendEmail(superAdmin, newUser.firstName, newUser.collectivity.name);
            var r = collectivityRequest.sendEmail(newUser, newUser.collectivity.name);

        }
    }
    catch (e) {
        //ds.rollBack();
        newCollectivity.remove();
        res.msg = "Une erreur inconnue est survenue lors de la création de compte, veuillez contacter l'administrateur";

    }
    finally {
        return res;
    }
};

model.Account.methods.login = function (user) {
    var res = {
        "status": false,
        "msg": "",
        "obj": {}
    };
    if (loginByPassword(user.userName, user.passwd) == false){
    	res.msg = "failed to connect"
        return res;
    }
    var currentUser = ds.Account.find("userName == :1", user.userName);
    if (currentUser.role.tab[0] != Constantes.superAdmin) {
        if (!currentUser.collectivity.active) {
            res.msg = "account_disabled";
            return res;
        }
        res.obj = {
            "fullName": currentUser.fullName,
            "zipcode": currentUser.collectivity.zipCode,
            "collectivity": currentUser.collectivity.name,
            "role": currentUser.role.tab[0],
            "date": currentUser.subDate
        };
    }
    else {
        res.obj = {
            "fullName": currentUser.fullName,
            "role": currentUser.role.tab[0],
            "date": currentUser.subDate
        };
    }

    res.status = true;
    res.msg = "login OK";
    return res;
};

model.Account.methods.getCurrentUser = function () {
    var res = {
        "status": false,
        "msg": "",
        "obj": {}
    };


    if (currentSession().user.name == "default guest") {
        return res;
    }
    var currentUser = ds.Account.find("userName == :1", currentSession().user.name);
    res.obj = {
        "firstName": currentUser.firstName,
        "lastName": currentUser.lastName,
        "userName": currentUser.userName,
        "role"    : currentUser.role.tab[0]
    };
    res.status = true;
    res.msg = "login OK";
    return res;
};

model.Account.methods.updateUser = function (user) {
    var res = {
        "status": false,
        "msg": "",
        "obj": {}
    };
    var currentUser = ds.Account.find("userName == :1", currentSession().user.name);
    if (!currentUser) {
    	res.msg = "There is no user logged in"
        return res;
    }
    var keyHa = currentUser.keyHa;
    if(directory.computeHA1(keyHa, user.old) != currentUser._password){
    	res.msg = "The current password is not valid"
        return res;
    }
    keyHa = randomString();
    currentUser._password = directory.computeHA1(keyHa, user.newPassword);
    currentUser.keyHa = keyHa;
    currentUser.save();
    res.status = true;
    res.msg = "update OK";
    return res;

};

model.Account.methods.logout = function () {
    var state = logout();
    return state;
};

model.Account.methods.findUserByEmail = function (email) {
	 var res = {
        "status": false,
        "msg": "",
        "obj": {}
    };    
    var user = ds.Account.find("userName = :1", email);
    if (!user) {
    	res.msg = "there is no user with this email"
    	return res
    }
    var token = randomString();
    var r = requestPassword.sendEmail(user, token);
    if(!r){
    	res.msg = "error occured when sending mail"
    	return res;
    }
    user.token_pwd = token;
    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    user.token_expire_date = tomorrow;
    user.save();
    res.msg = "user mail is found"
    res.obj = user.userName;
    res.status = true
    return res


};

model.Account.methods.findUserByToken = function (token, password) {
	var res = {
        "status": false,
        "msg": "",
        "obj": {}
    };
    
    user_token = ds.Account.find("token_pwd = :1", token);    
    if(!user_token){
    	res.msg = "Ce token n'existe pas";
    	return res
	}
	
    var keyHa = randomString();
    user_token._password = directory.computeHA1(keyHa, password);
    user_token.keyHa = keyHa;
    user_token.token_pwd = null;
    user_token.save();
    var r = changePassword.sendEmail(user_token);
    if(!r){
    	res.msg = "l'email n'est pas envoyé";
    }
   	res.status = true;
   	res.msg = "token found"
    return res;


};

model.Account.methods.isTokenValid = function (token) {
    var res = {
        "status": false,
        "msg": "",
        "obj": {}
    };
    user_token = ds.Account.find("token_pwd = :1", token);

    if(!user_token){
    	res.msg = "Ce token n'existe pas"
    	return res
    }
    date_end = user_token.token_expire_date.getTime();
    if (date_end < new Date()) {
    	user_token.token_pwd = "";
    	user_token.token_expire_date = null;
        res.msg = "Token date has expired"
    	return res 
	}
	res.status = true;
    res.msg = "Token is valid"
    return res;
};

model.Account.methods.countAdminAgent = function () {
    var res = {
        "admin": 0,
        "agent": 0,
        "service": 0,
        "collectivity": 0,
        "ticket": 0,
        "msg": ""
    };

    var accounts = ds.Account.all();
    var colllctivity = ds.Collectivity.all();
    var service = ds.Services.all();
    var ticket = ds.Ticket.all();

    var i;
    for (i = 0; i < accounts.length; i++) {
        if(accounts[i].role){
            if (accounts[i].role.tab[0] == Constantes.admin)
                res.admin++;
            if (accounts[i].role.tab[0] == Constantes.agent)
                res.agent++;
        }
    }
    res.collectivity = colllctivity.length;
    res.service = service.length;
    res.ticket = ticket.length;
    return res;
};


model.Account.methods.updateUser.scope = "public";
model.Account.methods.getCurrentUser.scope = "public";
model.Account.methods.addUser.scope = "public";
model.Account.methods.login.scope = "public";
model.Account.methods.logout.scope = "public";
model.Account.methods.isTokenValid.scope = "public";
model.Account.methods.findUserByEmail.scope = "public";
model.Account.methods.findUserByToken.scope = "public";
model.Account.methods.loginAdmin.scope = "public";
model.Account.methods.countAdminAgent.scope = "public";

function randomString() {
    var length = 15;
    var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var result = '';
    for (var i = length; i > 0; --i)
        result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}   
          
            




