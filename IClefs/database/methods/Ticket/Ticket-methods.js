var mail = require("../../../backend/modules/sendMail.js");
var Constantes = require("../../../backend/modules/const.js");

model.Ticket.entityMethods.Validate = function() {
	res = {
		status: false,
		msg: "",
		obj: {}
	};
	if(!this.collectivity){
		res.msg = "ce ticket n'appartient à aucune collectivité"
		return res
	}
	var collectivityName = this.collectivity.name;	
	var fullName = this.fcData.given_name+" "+this.fcData.family_name;
	var email = this.fcData.email;
    if(!this.service){
		res.msg = "ce ticket n'appartient à aucun service"
		return res
	}
    var serviceName = this.service.name;
    var num = this.ID;
    var mails = this.service.mailCollection;
	try {
		this.status = true;
		this.archive = true;
		this.save();
		var send = false;
		//send mail personaliser
		if(mails){
			mails.forEach(function (obj){
				if(obj.type == "acceptation"){
					send = true;
					/********************/
                    var name= /%nomComplet%/g;
                    var colectivity_name = /%nomCollectivite%/g;
                    var service_name = /%nomService%/g;
                    var numDossier = /%numDossier%/g;
                    var body = obj.body;
                    body = body.replace(name,fullName);
                    body = body.replace(colectivity_name,collectivityName);
                    body = body.replace(service_name,serviceName);
                    body = body.replace(numDossier,num);
					var r = mail.send(obj.subject, body, email);
                    /********************/
				}
			});
		}

		res.status = true;
		res.msg = "Validate OK";
		res.obj = this;


	}
	catch (e) {
		res.msg = e.message;
	}
	finally {
		return res;
	}
};

model.Ticket.entityMethods.refused = function() {
	res = {
		status: false,
		msg: "",
		obj: {}
	};
	if(!this.collectivity){
		res.msg = "ce ticket n'appartient à aucune collectivité"
		return res
	}
    var collectivityName = this.collectivity.name;
    var fullName = this.fcData.given_name+" "+this.fcData.family_name;
    var email = this.fcData.email;
    if(!this.service){
		res.msg = "ce ticket n'appartient à aucun service"
		return res
	}
    var serviceName = this.service.name;
    var mails = this.service.mailCollection;
    var num = this.ID;

	try {
		this.status = false;
		this.archive = true;
		this.save();
        //l'envoi mail personaliser
		var send = false;
        if(mails){
            mails.forEach(function (obj){
                if(obj.type == "refus"){
                    send = true;
					/********************/
                    var name= /%nomComplet%/g;
                    var colectivity_name = /%nomCollectivite%/g;
                    var service_name = /%nomService%/g;
                    var numDossier = /%numDossier%/g;
                    var body = obj.body;
                    body = body.replace(name,fullName);
                    body = body.replace(colectivity_name,collectivityName);
                    body = body.replace(service_name,serviceName);
                    body = body.replace(numDossier,num);
                    var r = mail.send(obj.subject, body, email);
					/********************/
                }
            });

        }

		res.status = true;
		res.msg = "Refused OK";
		res.obj = this;
	}
	catch (e) {
		res.msg = e.message;
	}
	finally {
		return res;
	}
};

model.Ticket.methods.findTicketByID = function(ID) {
    var res = {
        "status" : false,
        "msg" : "",
        "obj" : {}
    };

    var token = currentSession().promoteWith(Constantes.agent);
    var ticket = null;
	if(!ID){
		res.msg = "aucun ID n'est envoyé"	
	}
	else{
	    ticket = ds.Ticket.find("ID == :1",ID);
	    if(!ticket)
	         res.msg = "Ticket n'existe pas";
		else{
			res.status = true;
		    res.obj = ticket;
			res.msg = "ticket found"
		}
	}            
    res = JSON.parse(JSON.stringify(res));    
    currentSession().unPromote(token);
    return res;
};

model.Ticket.entityMethods.Validate.scope = "public";
model.Ticket.entityMethods.refused.scope = "public";
model.Ticket.methods.findTicketByID.scope = "public";


