
model.Profil.methods.addProfil = function(profil) {
	var res = {
		"status" : false,
		"msg" : "",
		"obj" : {}
	};
	var i = 0;
	var pop = 0;
	if (ds.Profil.find("name = :1", profil.name)){
		res.msg = "This Profil already exist";
		return res;
	}
	if (profil.name == null) {
		res.msg = "Empty string name";
		return res;
	}
	try {
		var newProfil = new ds.Profil();
		newProfil.name = profil.name;
		newProfil.collectivity = ds.Collectivity.find("name = :1", localStorage.access);
		newProfil.conf = {tab: profil.tab};
		newProfil.save();
		res.status = true;
		res.msg = "profilAdd OK";
		res.obj = newProfil;
		console.log(newProfil);
	}
	catch (e) {
		res.msg = e.message;
	}
	finally {
		return res;
	}
};

model.Profil.methods.addProfil.scope = "public";