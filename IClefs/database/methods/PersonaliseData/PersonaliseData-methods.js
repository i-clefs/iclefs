var Constantes = require("../../../backend/modules/const.js");

model.PersonaliseData.methods.addField = function(data,service) {
    var res = {
        status : false,
        msg : ""
    };

    if(data === null || service === null || Array.isArray(data) !== true || data.length === 0  || typeof(service) !== "object"){
        res.msg = "paramètre incorrecte";
        return res;
    }
    var ser = ds.Services.find("ID == :1", service.ID);
    if(!ser){
        res.msg = "service n'existe pas";
        return res;
    }
    data.forEach(function (obj) {
        if(typeof(obj) === "object"){
            var personaliseData = new ds.PersonaliseData();
            personaliseData.name = obj.name;
            personaliseData.type = obj._type;
            personaliseData._required = obj.isRequired;
            personaliseData.services = service;
            personaliseData.save();
        }
    });
    res.status = true;
    res.msg = "bien ajouter";

    return res;
};

model.PersonaliseData.methods.getFields = function(serviceID) {
	var res = {
	    status : false,
	    msg : "",
        obj : {}
    };

	if(serviceID === null || serviceID === ""){
	    res.msg = "paramètre incorrecte";
	    return res;
    }
    var token = currentSession().promoteWith(Constantes.agent);
    var service = ds.Services.find("ID == :1",serviceID);
    if(!service){
        res.msg = "service n'existe pas";
        return res;
    }
    if(!service.personaliseData || service.personaliseData.length === 0){
        res.msg = "ce service ne contient pas des données personnaliser";
        return res;
    }

    res.status = true;
    res.msg = "ce service contient des données personnaliser";
    res.obj = service.personaliseData;

    res = JSON.parse(JSON.stringify(res));
	  currentSession().unPromote(token);
	return res;
};


model.PersonaliseData.methods.addField.scope = "public";
model.PersonaliseData.methods.getFields.scope = "public";