var validateRequest = require("../../../backend/modules/emails/collectivity/validateRequest/collectivity.validateRequest.js");
var refuseRequest = require("../../../backend/modules/emails/collectivity/refuseRequest/collectivity.refuseRequest.js");

model.Collectivity.entityMethods.setClient = function(data) {
    var res = {
        status : false,
        msg : ""
    };
    if(!data){
        res.msg = "paramètre erroné";
        return res;
    }
    if (data.client_Id)
        this.client_Id = data.client_Id;
    if (data.client_secret)
        this.client_secret = data.client_secret;
    if (data.name)
        this.name = data.name;
    if (data.zipCode)
        this.zipCode = data.zipCode;
    this.postRedirectUri = data.postRedirectUri;
    try {
        this.save();
        res.status = true;
        res.msg = "la mise à jour est faite";
    }
    catch (e) {
        res.msg = e.message;
    }
    finally {
        return res;
    }
};


model.Collectivity.methods.valideRefuse = function(accountID,collectivityID,operation) {
    var res = {
        status : false,
        msg : ""
    };

    if(!accountID && !collectivityID){
        res.msg = "paramètre erroné";
        return res;
    }

    var account = ds.Account.find('ID = :1',accountID);
    var community = ds.Collectivity.find('ID = :1',collectivityID);
    if(!account && !community){
        res.msg = "compte n'existe pas";
        return res;
    }
    if(operation == "valide"){
        var r = validateRequest.sendEmail(account,community.name);
        community.active = true;
        community.save();
        res.status = true;
        res.msg = "la demande est validée";
    }
    if(operation == "refuse"){
        var r = refuseRequest.sendEmail(account,community.name);
        community.remove();
        account.remove();
        res.status = true;
        res.msg = "la demande est refusé";
    }

    return res;
};

model.Collectivity.entityMethods.setClient.scope = "public";
model.Collectivity.methods.getDemandes.scope = "public";
model.Collectivity.methods.valideRefuse.scope = "public";



