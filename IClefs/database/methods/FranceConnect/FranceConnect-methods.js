model.FranceConnect.methods.FC_authorize = function(Info)
{
    if (Info.client_id == null || Info.client_secret == null)
    {
        return ("Error");
    }
    var client = {
        "authorize": "https://fcp.integ01.dev-franceconnect.fr/api/v1/authorize",
        "conf": "openid%20profil%20birth",
        "client_id": Info.client_id,// attention ds du client_id etc
        "client_secret": Info.client_secret,// attention ds du client_id etc
        "redirect_uri": "http%3A%2F%2Ffc.wakanda.io",
        "baseUrl": "http%3A%2F%2Ffc.wakanda.io",
        "scope": "openid email",
        "authentication": true
    };
	Info.zipcode = "123456789";
    var url = client.authorize + "?response_type=code&client_id=" + client.client_id + "&redirect_uri=" + client.redirect_uri + "&scope=" + client.conf + "&state="+ Info.zipcode + "&nonce=123456789";
    return (url);
};
model.FranceConnect.methods.FC_authorize.scope = 'public';


model.FranceConnect.methods.FC_token = function(URLCallback)
{
	var CodeURLFC = get_param_in_url(URLCallback, "code");
	var state = get_param_in_url(URLCallback, "state");
    var xhr = new XMLHttpRequest();
    var body = formBodyFromJSON({
        'code': CodeURLFC,
        'client_id': client.client_id, // attention ds du client_id etc
        'client_secret': client.client_secret, // attention ds du client_id etc
        'redirect_uri': client.redirect_uri, // attention ds du client_id etc
        'grant_type': 'authorization_code'
    });

    xhr.open('POST', 'https://fcp.integ01.dev-franceconnect.fr/api/v1/token', true);
    xhr.onreadystatechange = function(event)
    {
        if (xhr.readyState == 4 && xhr.status == 200)
        {
            var reponse = JSON.parse(xhr.responseText);
            console.log(reponse);
            var Info = {};
            Info.Token = reponse.access_token;
            Info.Token_id = reponse.id_token;
            Info.type = reponse.token_type;
            Info.time = reponse.expires_in;
            Info.state = state;
            return (Info); // return
        }
    }
    xhr.send(body);
    return (Info); // return le bon truc
};
model.FranceConnect.methods.FC_token.scope = 'public';

model.FranceConnect.methods.FC_UserInfo = function(Info) {
	var xhr = new XMLHttpRequest();
	
	xhr.open("GET" , "https://fcp.integ01.dev-franceconnect.fr/api/v1/userinfo/api/v1/userinfo?schema=openid", true);
	xhr.setRequestHeader("Authorization", "Bearer " + Info.Token);
	xhr.onreadystatechange = function()
	{
		if (xhr.readyState == 4 && xhr.status == 200)
		{
			reponse = JSON.parse(xhr.reponseText);
			console.log(reponse); // a return
		}
	}
	xhr.send();
};

model.FranceConnect.methods.FC_UserInfo.scope = 'public';


model.FranceConnect.methods.FC_logout = function(Info) {
var xhr = new XMLHttpRequest();
	
	xhr.open("GET" , "https://fcp.integ01.dev-franceconnect.fr/api/v1/logout?id_token_hint=" + Info.Token_id+ "&state=" + Info.State + "&post_logout_redirect_uri=" + cite_colectivité); // change par le ds
	xhr.onreadystatechange = function()
	{
		if (xhr.readyState == 4 && xhr.status == 200)
		{
			reponse = JSON.parse(xhr.reponseText);
			console.log(reponse); // a return
		}
	}
	xhr.send();
};
model.FranceConnect.methods.FC_logout.scope = 'public';

model.FranceConnect.methods.API_particulier = function(Info)
{
    var xhr = new XMLHttpRequest();
    if (Info.scope == "impots")
    {
    	Info.url = Info.scope + "/";
    	if (Info.scope2 == "svair")
	    	Info.url = Info.url + Info.scope2 + "/";
	    else if (Info.scope2 == "adress")
	    	Info.url = Info.url + Info.scope + "/";
	    else
	    	return ("Error URL scope");
	    Info.url = Info.url + "?" + "numeroFiscal=" + Info.numeroFiscal + "&referenceAvis=" + Info.referenceAvis;
    }
    else if (Info.scope == "caf")
    {
    	Info.url = Info.scope + "/";
    	if (Info.scope2 == "qf")
    		Info.url = Info.url + Info.scope + "/";
    	else if (Info.scope2 == "adresse")
    		Info.url = Info.url + Info.scope2 + "/";
    	else if (Info.scope2 == "famille")
    		Info.url = Info.url + Info.scope2 + "/";
    	else
    		return ("Error URL scope");
    	Info.url = Info.url + "?" + "codePostal=" + Info.codePostal + "&numeroAllocataire=" + Info.numeroAllocataire; 
    }
    else
    	return ("error URL");
    Info.url = Info.url + "?";
    
	var url = "https://particulier.api.gouv.fr/api/";
	
    xhr.open("GET", url + Info.url, true);
    xhr.setRequestHeader('Content-type', 'application/json');
	xhr.setRequestHeader('X-API-KEY', "TOKEN");
	xhr.setRequestHeader('X-USER', "USER");
	
    xhr.onreadystatechange = function(event)
    {
        if (xhr.readyState == 4 && xhr.status == 200)
        {
            if (reponse == null)
            {
                console.log("reponse == NULL");
            }
            else
            {
                var reponse = JSON.parse(xhr.reponseText);
                console.log(reponse); // a return 
            }
        }
    }
    xhr.send(null);
};
model.FranceConnect.methods.API_particulier.scope = 'public';

function get_param_in_url(URL, param)
{
	var split = URL.split("/");
	split = split[split.length - 1];
	var idex;
	if ((idex = split.indexOf(param)) == -1)
		return ("error");
	var length = split.length;
	var first_step = split.slice(idex, length);
	var tab_first = first_step.split("&");
	var idex_first = 0;
	while (tab_first[idex_first].search(param) == -1 && idex_first != tab_first.length - 1)
	{
		idex_first++;
	}
	var param_str = tab_first[idex_first];
	var param_tab = param_str.split("=");
	var param = param_tab[1];
	return (param);
}

function formBodyFromJSON( params )
{
	var body = '';
	for ( var key in params )
	{
		body += body.length == 0 ? '' : '&';
		body += key +'='+ encodeURIComponent( params[ key ] );
	}
	return body;
}