

model.Mail.methods.addMail = function(mails,service) {
    var res = {
        status : false,
        msg : ""
    };
    if(mails === null || Array.isArray(mails) === false || service === null || typeof(service) !== "object"){
        res.msg = "paramètre incorrecte";
        return res;
    }
    var ser = ds.Services.find("ID == :1",service.ID);
    if(!ser){
        res.msg = "Service n'existe pas";
        return res;
    }

	mails.forEach(function (obj) {
	    if(typeof(obj) === "object"){
            var mail = new ds.Mail();
            mail.subject = obj.subject;
            mail.body = obj.body;
            mail.type = obj._type;
            mail.service = ser;
            mail.save();
        }
    });

    res.status = true;
    res.msg = "mails bien crée";
    return res;
};


model.Mail.methods.updateMail = function(mail,serviceId) {
    var res = {
        status : false,
        msg : "",
        obj : {}
    };

    if(typeof(mail) !== "object" || serviceId === null || serviceId === ""){
        res.msg = "paramètre incorrecte";
        return res;
    }
    if(!(mail.type && mail.body && mail.subject && mail.ID)){
        res.msg = "champs vides";
        return res;
    }
    var service = ds.Services.find("ID == :1",serviceId);
    if(!service){
        res.msg = "service n'existe pas";
        return res;
    }
    var currentMail = ds.Mail.find("ID == :1",mail.ID);
    if(!currentMail){
        res.msg = "mail n'existe pas";
        return res;
    }

    currentMail.subject = mail.subject;
    currentMail.body = mail.body;
    currentMail.save();
    res.status = true;
    res.msg = "la mise à jour est faite";

    return res;
};


model.Mail.methods.addMail.scope = "public";
model.Mail.methods.updateMail.scope = "public";



