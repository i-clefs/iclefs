
include("./methods/Account/Account-methods.js");
include("./methods/S2low/S2low-methods.js");
include("./methods/FranceConnect/FranceConnect-methods.js");
include("./methods/Account/Account-events.js");
include("./methods/Services/Services-methods.js");
include("./methods/Collectivity/Collectivity-methods.js");
include("./methods/Profil/Profil-methods.js");
include("./methods/Ticket/Ticket-methods.js");
include("./methods/Collectivity/Collectivity-events.js");
include("./methods/Mail/Mail-methods.js");
include("./methods/PersonaliseData/PersonaliseData-methods.js");
include("./methods/DataValue/DataValue-methods.js");