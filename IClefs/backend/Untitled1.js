var Constantes = require("./modules/const.js");
//function thisGuyBecomeAnAdmin(userName) {
//	var user = ds.Account.find("userName = :1", userName);
//	if (user) {
//		try {
//			user.role.tab.push("Admin de collectivité");
//			console.log(user);
//			user.save();
//		}
//		catch (e) {
//			console.log(e.message);
//		}
//	}
//}

//thisGuyBecomeAnAdmin("jean-adrien.domage@wakanda.io");

//var fcTools = require("./modules/oauth2-provider-FranceConnect");

//Info = {
//	scope: "impots",
//	scope2: "svair",
//	numeroFiscal: "12",
//	referenceAvis: "15"
//}

//var ret = fcTools.getInfoApiParticulier(Info);
//console.log(ret);

function createSuperAdmin(request, response) {

    var token = currentSession().promoteWith(Constantes.agent);
    var admin = ds.Account.query("role.tab[0] == :1", Constantes.superAdmin);
    if(admin){
        currentSession().unPromote(token);
        return "ce compte existe déjà";
    }

	var newUser = new ds.Account();
	newUser.userName = "superAdmin@iclefs.io";
	var keyHa = randomString();
	newUser.keyHa = keyHa;
	newUser._password = directory.computeHA1(keyHa, "Super Admin");
	newUser.subDate = new Date();
	newUser.firstName = "Admin";
	newUser.lastName = "Global";
	newUser.role = {tab : [Constantes.superAdmin]};
	newUser.status = true;
	newUser.save();
    currentSession().unPromote(token);
	return "ok";

}
		
function randomString() {
    var length = 15;
    var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var result = '';
    for (var i = length; i > 0; --i)
        result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}   
   