var mailTicket = require("../modules/emails/ticket/ticket.request.js");
var Constantes = require("../modules/const.js");

function DgfipCallBack (request, response) {
    InfoApi = {
        scope: "impots",
        scope2: "svair",
        numeroFiscal: JSON.parse(request.body).numFiscal,
        referenceAvis: JSON.parse(request.body).refAvis
    };
    var ID = 0;

    if(sessionStorage.getItem("fcData") === null){
        response.body = JSON.stringify({"messageText" : "identifiant pivote n'est pas reçu", "ticketID" : 0});
        return response;
    }
    var token = currentSession().promoteWith(Constantes.agent);
    try {
        var res = getInfoApiParticulier(InfoApi);
        if (res.status === false){
            response.body = JSON.stringify({"messageText" : res.msg, "ticketID" : 0});
            return response;
        }
        sessionStorage.setItem("apiData",res.obj);
        var service = ds.Services.find("ID = :1", JSON.parse(request.body).refService);
        if(!service) {
            response.body = JSON.stringify({"messageText" : "service n'existe pas", "ticketID" : 0});
            return response;
        }
        if(!service.config.adresse && !service.config.quotientFamilial && !service.config.compositionFamiliale && service.personaliseData.length == 0){
                var newTicket = new ds.Ticket();
                newTicket.service = service;
                newTicket.collectivity = service.collectivity;
                newTicket.fcData = sessionStorage.getItem("fcData");
                newTicket.apiData = sessionStorage.getItem("apiData");
                newTicket.service.save();
                newTicket.date = new Date();
                newTicket.status = false;
                newTicket.archive = false;
                newTicket.save();
                res.msg = "ticket saved";
                ID = newTicket.ID;
                var r = mailTicket.sendEmail(newTicket, service.collectivity.name, service.name);
                sessionStorage.clear();
            }

        response.statusCode = res.statusCode;
        response.body = JSON.stringify({"messageText" : res.msg, "ticketID" : ID});

    }
    catch (e) {
        response.statusCode = 400;
        response.message = "Une erreur est survenue";
    }

    currentSession().unPromote(token);
    return response;

}

getInfoApiParticulier = function(Info)
{
    var res = {
        status: false,
        statusCode  : 0,
        msg: "",
        obj:{}
    }
    var xhr = new XMLHttpRequest();

    var url = "https://particulier-sandbox.api.gouv.fr/api/" + Info.scope + "/" + Info.scope2+ "?" + "numeroFiscal=" + Info.numeroFiscal + "&referenceAvis=" + Info.referenceAvis;


    // changer pour la vrai URL
    var response;
    xhr.open("GET", url);
    xhr.setRequestHeader('Content-type', 'application/json');

    xhr.setRequestHeader('X-API-KEY', "test-token"); // changer pour le vrai token
    xhr.setRequestHeader('X-USER', "demo"); // changer pour le vrai USER

    xhr.onreadystatechange = function(event){
        res.statusCode = xhr.status;
        if (xhr.readyState == 4 && xhr.status == 200){
            response = JSON.parse(xhr.responseText);
        }
        else if (xhr.status != 200){
            res.msg = xhr.responseText;
            if(xhr.responseText){
                response = JSON.parse(xhr.responseText);
            }
        }
    }
    xhr.send();
    if (response){
        if(response.error != null){
            res.msg = response.reason;
        }
        else{
            res.status = true;
            res.obj = response;
        }
    }
    return res;
}


