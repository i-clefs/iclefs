var Constantes = require("../modules/const.js");

function cancelRequest(request, response) {

    var serviceID = JSON.parse(request.body).serviceID;
    var uri = Constantes.uri;
    var redirectURL = uri+"/app/#!callback_page";
    var token = currentSession().promoteWith(Constantes.agent);

    sessionStorage.clear();
    var service = ds.Services.find("ID = :1", serviceID);
    if(service){
        if(service.collectivity.postRedirectUri != null)
            redirectURL = service.collectivity.postRedirectUri;
    }
	currentSession().unPromote(token);
    response.body = JSON.stringify({"redirectURL" : redirectURL});
}