var mailTicket = require("../modules/emails/ticket/ticket.request.js");
var Constantes = require("../modules/const.js");

function uploadFile(request, response) {

    var res = [];
    if(!request.urlQuery){
        res.msg = "empty params";
        return JSON.stringify(res);
    }
    var token = currentSession().promoteWith(Constantes.agent);
    var params = getParams(request.urlQuery);
    var serviceID = params[1];
    //var fieldID = params[0];
    var service = ds.Services.find("ID = :1", serviceID);
    //var field = ds.PersonaliseData.find("ID == :1",fieldID);
    if((!service) && (!field)){
        res.msg = "empty object";
        return JSON.stringify(res);
    }

    var newTicket = new ds.Ticket();
    newTicket.service = service;
    newTicket.collectivity = newTicket.service.collectivity;
    newTicket.fcData = sessionStorage.getItem("fcData");
    if(sessionStorage.getItem("apiData"))
        newTicket.apiData = sessionStorage.getItem("apiData");
    if(sessionStorage.getItem("cafData"))
        newTicket.cafData = sessionStorage.getItem("cafData");
    newTicket.service.save();
    newTicket.date = new Date();
    newTicket.status = false;
    newTicket.archive = false;
    newTicket.save();
    sessionStorage.clear();
    var r = mailTicket.sendEmail(newTicket, service.collectivity.name, service.name);

    for(var i=0;i<request.parts.length;i++){
        var fieldID = request.parts[i].name.split("_")[1];
        var field = ds.PersonaliseData.find("ID == :1",fieldID);
        var extension = request.parts[i].fileName.split('.')[request.parts[i].fileName.split('.').length-1].toLowerCase();
        var obj = {
            'status' : false,
            'msg' : "",
            'filed' : fieldID,
            'ticketID' : newTicket.ID
        };

        // accepter just les fichiers jpg,png,txt,pdf et doc
        var fileName = saveFile(request,response,i);
        //sauvgarder les données dans la table DataValue

        var newDataValue = new ds.DataValue();
        newDataValue.valeur = fileName;
        newDataValue.ticket = newTicket;
        newDataValue.personnaliseData = field;
        newDataValue.save();
        obj.status = true;
        obj.msg = "seved";
        console.log("file saved with ticket");
        res.push(obj);
    }

    currentSession().unPromote(token);

    return JSON.stringify(res);

}


//fonction pour extraire les parametres de requête
function getParams(str) {
    //var str = "field=5&&tID=8";
    var tmp = str.split("&&");
    var tab = [];
    for(var i=0;i<tmp.length;i++){
        tab[i] = tmp[i].split("=")[1];
    }
    return tab;
}


//Fonction pour sauvgarder le fichier dans le dossier ==> /PROJECT/database/data/tmp/
function saveFile(rq,rp,index){
    var err = 0;
    try {
        var counter = 1;
        var nameTemp;
        var files = [];
        var uploadFolder = Folder('/PROJECT/database/data/tmp/');
        var result = [];
        var newName;
        var myBinaryStream;


        // create upload folder if not existing
        if (!uploadFolder.exists) {
            uploadFolder.create();
        }

        //return request.parts[0].;
        // create file instances
        //for (var i = 0; i < rq.parts.length; i++) {
        err = 1;
        files.push(new File(uploadFolder.path + rq.parts[index].fileName.replace(/\s/g, '_')));
        // create result object
        result[0] = {};
        result[0].name = rq.parts[index].fileName;
        result[0].type = rq.parts[index].mediaType;
        result[0].size = rq.parts[index].size;

        //}
        // write file content
        //for (var i = 0; i < files.length; i++) {
        err = 2;
        counter = 1;
        if (!files[0].exists) {
            err = 3;
            //erreur dans cette fontion
            myBinaryStream = BinaryStream(files[0], "Write");
            myBinaryStream.putBlob(rq.parts[index].asBlob);
            myBinaryStream.close();
        } else {
            while (files[0].exists) {
                err = 4;
                nameTemp = files[0].name.replace(/\s/g, '_');
                files[0] = new File(uploadFolder.path + files[0].nameNoExt.replace(/\s/g, '_') + counter + '.' + files[0].extension);
                newName = files[0].name;
                if (files[0].exists) {
                    err = 5;
                    files[0] = new File(uploadFolder.path + nameTemp);
                }
                counter++;
            }
            err = 6;
            myBinaryStream = BinaryStream(files[0], 'Write');
            myBinaryStream.putBlob(rq.parts[index].asBlob);
            myBinaryStream.close();
            result[0].name = newName;
        }
        //}
        //result = JSON.stringify(result);
        err = 7;
        // add response header
        rp.contentType = 'application/json';

        return result[0].name;
    } catch (e) {
        console.log(e.stack);
        return e+" $ "+err;
    }
}
