var Constantes = require("../modules/const.js");

function generateCSV(request, response){

    var res = {
        "status": false,
        "msg": "",
        "obj": {}
    };
    var data = [];
    var fields = [];
    var className = JSON.parse(request.body).class;

    var currentUser = ds.Account.find("userName == :1", currentSession().user.name);

    if (currentSession().user.name == "default guest") {
        res.msg = "session expirée veuillez vous connecter a nouveau";
        return JSON.stringify(res);
    }
    if(className === "Account"){
        if(currentUser.role.tab[0] === Constantes.superAdmin){
            data = ds.Account;
            fields = ["ID","userName","firstName","lastName","role","subDate"];
        }
        else if(currentUser.role.tab[0] === Constantes.admin){
            data = ds.Account.query("collectivity.name == :1",currentUser.collectivity.name);
            fields = ["ID","local_government_id","first_name","last_name","email","password","local_government_admin","active"];
        }
        else{
            res.msg = "vous n'avez pas le droit d'acceder a cette fonctionnalité";
            return JSON.stringify(res);
        }
    }
    else if(className === "Demande des citoyens"){
        if(currentUser.role.tab[0] === Constantes.admin){
            data = ds.Ticket.query("collectivity.name == :1",currentUser.collectivity.name);
            fields = ["ID","date","state","france_connect_identite_pivot_nom","france_connect_identite_pivot_prenom","france_connect_identite_pivot_email","france_connect_identite_pivot_adresse",
            "france_connect_identite_pivot_sexe","france_connect_identite_pivot_date_naissance","france_connect_identite_pivot_lieu_naissance","dgfip_situation_familiale","dgfip_annee_imposition",
            "dgfip_annee_revenu","dgfip_nombre_parts","dgfip_nombre_personnes_a_charge","dgfip_revenu_brut_global","dgfip_revenu_revenu_fiscal_reference","dgfip_revenu_revenu_imposable",
            "caf_quotient_familial","caf_adresse_rue","caf_adresse_code_postal","caf_adresse_pays"];
        }
        else {
            res.msg = "vous n'avez pas le droit d'acceder a cette fonctionnalité";
            return JSON.stringify(res);
        }
    }
    else if(className === "Enfants"){
        if(currentUser.role.tab[0] === Constantes.admin){
            var data = ds.Ticket.query("collectivity.name == :1",currentUser.collectivity.name);
            fields = ["demande_id","first_name","last_name","sexe","date_naissance"];
            var CSV = getChilds(data,fields);
            res.status = true;
            res.obj.data = fields.join(',');
            res.obj.data += "\n"+CSV;

            return JSON.stringify(res);
        }
        else {
            res.msg = "vous n'avez pas le droit d'acceder a cette fonctionnalité";
            return JSON.stringify(res);
        }
    }
    else if(className === "Parents"){
        if(currentUser.role.tab[0] === Constantes.admin){
            data = ds.Ticket.query("collectivity.name == :1",currentUser.collectivity.name);
            var tmp = [];
            for(var i=0;i<data.length;i++){
                if(data[i].apiData && data[i].apiData.declarant1 && data[i].apiData.declarant2){
                    tmp.push(data[i]);
                    console.log(tmp);
                }

            }
            fields = ["demande_id","first_name","last_name","date_naissance"];
            var CSV = getParents(tmp,fields);
            res.status = true;
            res.obj.data = fields.join(',');
            res.obj.data += "\n"+CSV;

            return JSON.stringify(res);
        }
        else {
            res.msg = "vous n'avez pas le droit d'acceder a cette fonctionnalité";
            return JSON.stringify(res);
        }
    }
    else if(className === "Services"){
        if(currentUser.role.tab[0] === Constantes.admin){
            data = ds.Services.query("collectivity.name == :1",currentUser.collectivity.name);
            fields = ["ID","local_government_id","name","france_connect_identite_pivot","dgfip_avis_imposition","dgfip_adresse_fiscale","caf_quotient_familial","caf_composition_famille","caf_adresse","email_accuse_reception_sujet","email_accuse_reception_texte","email_acceptation_sujet","email_acceptation_texte","email_refus_sujet","email_refus_texte","state","france_connect_button_html_code"];
        }
        else {
            res.msg = "vous n'avez pas le droit d'acceder a cette fonctionnalité";
            return JSON.stringify(res);;
        }
    }
    else if(className === "Collectivity"){
        /*if(currentUser.role.tab[0] === "Super Admin"){
            data = ds.Collectivity.query("active == :1",false);
            fields = ["ID","name","numero_cnil","zip_code","france_connect_client_id","france_connect_secret_key","redirect_url"];
        }*/
        if(currentUser.role.tab[0] === Constantes.admin){
            data = ds.Collectivity.query("name == :1", currentUser.collectivity.name);
            fields = ["ID","name","numero_cnil","zip_code","france_connect_client_id","france_connect_secret_key","redirect_url"];
        }
        else{
            res.msg = "vous n'avez pas le droit d'acceder a cette fonctionnalité";
            return JSON.stringify(res);
        }
    }
    else if(className === "statistics"){
        if(currentUser.role.tab[0] === Constantes.admin){
            data = ds.Services.query("collectivity.name == :1",currentUser.collectivity.name);
            fields = ["ID","name","status","totalRequests","Requests_of_the_week","Requests_of_the_day"];
        }
        else{
            res.msg = "vous n'avez pas le droit d'acceder a cette fonctionnalité";
            return JSON.stringify(res);
        }
    }
    else if(className === "Champs personnalisés"){
        if(currentUser.role.tab[0] === Constantes.admin){
            data = ds.PersonaliseData.query("services.collectivity.name == :1",currentUser.collectivity.name);
            fields = ["ID","service_id","custom_field_name","custom_field_type","custom_field_mandatory"];
        }
        else{
            res.msg = "vous n'avez pas le droit d'acceder a cette fonctionnalité";
            return JSON.stringify(res);
        }
    }
    else{
        res.msg = "désolé! impossible de faire cette demande"
        return JSON.stringify(res);
    }



    var CSV = getCSVfromData(data,fields,className);
    res.status = true;
    res.obj.data = fields.join(',');
    res.obj.data += "\n"+CSV;

    return JSON.stringify(res);
}


function getCSVfromData(data,fields,className){

    var x = data.toArray().map(function (one){
        return fields.map(function(attr){
            var res = "";
            if(className === "Account"){
                switch (attr){
                    case "local_government_id":
                        res = one["collectivity"].__KEY;
                        break;
                    case "first_name":
                        res = one["firstName"];
                        break;
                    case "last_name":
                        res = one["lastName"];
                        break;
                    case "email":
                        res = one["userName"];
                        break;
                    case "password":
                        res = one["_password"];
                        break;
                    case "local_government_admin":
                        if(one["role"].tab[0] === Constantes.admin)
                            res = true;
                        else
                            res = false;
                        break;
                    case "active":
                        res = one["status"];
                        break;
                    default:
                        res = typeof one[attr] === 'undefined'? '': one[attr];
                }
            }
            else if(className === "Demande des citoyens"){
                switch (attr){
                    case "state":
                        if(one["archive"] === false)
                            res = "en cours";
                        else if(one["status"] === true)
                            res = "validée";
                        else
                            res = "refusée";
                        break;
                    case "france_connect_identite_pivot_nom":
                        res = one["fcData"].family_name;
                        break;
                    case "france_connect_identite_pivot_prenom":
                        res = one["fcData"].given_name;
                        break;
                    case "france_connect_identite_pivot_email":
                        res = one["fcData"].email;
                        break;
                    case "france_connect_identite_pivot_adresse":
                        if(!one["fcData"].address){
                            res = "";
                        }
                        else if(typeof(one["fcData"].address) == "object")
                            res = one["fcData"].address.formatted;
                        else
                            res = one["fcData"].address;
                        break;
                    case "france_connect_identite_pivot_sexe":
                        res = one["fcData"].gender;
                        break;
                    case "france_connect_identite_pivot_date_naissance":
                        res = one["fcData"].birthdate;
                        break;
                    case "france_connect_identite_pivot_lieu_naissance":
                        res = one["fcData"].birthcountry;
                        break;
                    case "dgfip_situation_familiale":
                        if(one["apiData"])
                            if(one["apiData"] && one["apiData"].anneeImpots)
                            res = one["apiData"].anneeImpots;
                        else
                            res = "";
                        break;
                    case "dgfip_annee_revenu":
                        if(one["apiData"] && one["apiData"].anneeRevenus)
                            res = one["apiData"].anneeRevenus;
                        else
                            res = "";
                        break;
                    case "dgfip_nombre_parts":
                        if(one["apiData"] && one["apiData"].nombreParts)
                            res = one["apiData"].nombreParts;
                        else
                            res = "";
                        break;
                    case "dgfip_nombre_personnes_a_charge":
                        if(one["apiData"] && one["apiData"].nombrePersonnesCharge)
                            res = one["apiData"].nombrePersonnesCharge;
                        else
                            res = "";
                        break;
                    case "dgfip_revenu_brut_global":
                        if(one["apiData"] && one["apiData"].revenuBrutGlobal)
                            res = one["apiData"].revenuBrutGlobal;
                        else
                            res = "";
                        break;
                    case "dgfip_revenu_revenu_fiscal_reference":
                        if(one["apiData"] && one["apiData"].revenuFiscalReference)
                            res = one["apiData"].revenuFiscalReference;
                        else
                            res = "";
                        break;
                    case "caf_quotient_familial":
                        if(one["cafData"])
                            res = one["cafData"].quotientFamilial;
                        else
                            res = "";
                        break;
                    case "caf_adresse_rue":
                        if(one["cafData"])
                            res = one["cafData"].adresse.numeroRue;
                        else
                            res = "";
                        break;
                    case "caf_adresse_code_postal":
                        if(one["cafData"])
                            res = one["cafData"].adresse.codePostalVille;
                        else
                            res = "";
                        break;
                    case "caf_adresse_pays":
                        if(one["cafData"])
                            res = one["cafData"].adresse.pays;
                        else
                            res = "";
                        break;
                    default:
                        res = typeof one[attr] === 'undefined'? '' : one[attr] == null ? '' : one[attr];
                }
            }
            else if(className === "Services"){
                var mails = ds.Mail.query("service.ID == :1",one["ID"]);
                switch (attr){
                    case "local_government_id":
                        res = one["collectivity"].__KEY;
                        break;
                    case "france_connect_identite_pivot":
                        res = one["config"].identifiant;
                        break;
                    case "dgfip_avis_imposition":
                        res = one["config"].avisImposition;
                        break;
                    case "dgfip_adresse_fiscale":
                        res = one["config"].adresseFiscale;
                        break;
                    case "caf_quotient_familial":
                        res = one["config"].quotientFamilial;
                        break;
                    case "caf_composition_famille":
                        res = one["config"].compositionFamiliale;
                        break;
                    case "caf_adresse":
                        res = one["config"].adresse;
                        break;
                    case "email_accuse_reception_sujet":
                        for(var i=0;i<mails.length;i++){
                            if(mails[i].type === "demande")
                                res = mails[i].subject;
                        }
                        break;
                    case "email_accuse_reception_texte":
                        for(var i=0;i<mails.length;i++){
                            if(mails[i].type === "demande")
                                res = mails[i].body;
                        }
                        break;
                    case "email_acceptation_sujet":
                        for(var i=0;i<mails.length;i++){
                            if(mails[i].type === "acceptation")
                                res = mails[i].subject;
                        }
                        break;
                    case "email_acceptation_texte":
                        for(var i=0;i<mails.length;i++){
                            if(mails[i].type === "acceptation")
                                res = mails[i].body;
                        }
                        break;
                    case "email_refus_sujet":
                        for(var i=0;i<mails.length;i++){
                            if(mails[i].type === "refus")
                                res = mails[i].subject;
                        }
                        break;
                    case "email_refus_texte":
                        for(var i=0;i<mails.length;i++){
                            if(mails[i].type === "refus")
                                res = mails[i].body;
                        }
                        break;
                    case "state":
                        res = one["status"];
                        break;
                    case "france_connect_button_html_code":
                        res = one["codeBoutton"];
                        break;
                    default:
                        res = typeof one[attr] === 'undefined'? '': one[attr];
                }

            }
            else if(className === "Collectivity"){
                var user = ds.Account.query("collectivity.ID == :1",one["ID"])[0];
                switch (attr){
                    case "numero_cnil":
                        res = one["CNIL"];
                        break;
                    case "zip_code":
                        res = one["zipCode"];
                        break;
                    case "france_connect_client_id":
                        if(one["client_Id"])
                            res = one["client_Id"];
                        else
                            res = "";
                        break;
                    case "france_connect_secret_key":
                        if(one["client_secret"])
                            res = one["client_secret"];
                        else
                            res = "";
                        break;
                    case "redirect_url":
                        if(one["postRedirectUri"])
                            res = one["postRedirectUri"];
                        else
                            res = Constantes.uri;
                        break;
                    default:
                        res = typeof one[attr] === 'undefined'? '': one[attr];
                }
            }
            else if(className === "statistics"){
                var Tickets = ds.Ticket.query("service.name == :1",one["name"]);
                var ONE_DAY = 60 * 60 * 1000 * 24;
                var ONE_WEEK = ONE_DAY * 7;
                switch (attr){
                    case "status":
                        if(one["status"])
                            res = "Active";
                        else
                            res = "Inactive";
                        break;
                    case "totalRequests":
                        res = Tickets.length;
                        break;
                    case "Requests_of_the_week":
                        var weeklyState = 0;
                        for(var i=0;i<Tickets.length;i++){
                            if (((new Date) - new Date(Tickets[i].date)) < ONE_WEEK)
                                weeklyState++;
                        }
                        res = weeklyState;
                        break;
                    case "Requests_of_the_day":
                        var dailyState = 0;
                        for(var i=0;i<Tickets.length;i++){
                            if (((new Date) - new Date(Tickets[i].date)) < ONE_DAY)
                                dailyState++;
                        }
                        res = dailyState;
                        break;
                    default:
                        res = typeof one[attr] === 'undefined'? '': one[attr];
                }
            }
            else if(className === "Champs personnalisés"){
                switch (attr){
                    case "service_id":
                        res = one["services"].__KEY;
                        break;
                    case "custom_field_name":
                        res = one["name"];
                        break;
                    case "custom_field_type":
                        res = one["type"];
                        break;
                    case "custom_field_mandatory":
                        res = one["_required"];
                        break;
                    default:
                        res = typeof one[attr] === 'undefined'? '': one[attr];
                }
            }

            res = res.toString();
            if(res.lastIndexOf(',') >= 0){
                res = '"' + res + '"'
            }

            return res;
        }).join(',');
    });
    CSV = x.join("\n");
    return CSV;
}

function getChilds(data,fields){
    var CSV = "";
    var ticket = data.toArray().map(function (ticket){
    if(ticket["cafData"] && ticket["cafData"].enfants.length >0 ){
        var ch = ticket["cafData"].enfants.map(function (child){
            return fields.map(function (attr) {
                res = "";
                //["demande_id","first_name","last_name","sexe",""];
                switch (attr){
                    case "demande_id":
                        res = ticket.ID;
                        break;
                    case "first_name":
                        res = child["nomPrenom"].split(' ')[1];
                        break;
                    case "last_name":
                        res = child["nomPrenom"].split(' ')[0];
                        break;
                    case "sexe":
                        res = child["sexe"];
                        break;
                    case "date_naissance":
                        var D = child["dateDeNaissance"].toString().split("");
                        res = new Date(D.slice(0,2).join("")+"/"+ D.slice(2,4).join("")+"/"+ D.slice(4,8).join(""));
                        break;
                    default:
                        res = "";
                }

                res = res.toString();
                if(res.lastIndexOf(',') >= 0){
                    res = '"' + res + '"'
                }

                return res;
            }).join(',');
        });
        line = ch.join("\n");
        CSV += line+"\n";
    }
    });

    return CSV
}

function getParents(data,fields){
    var CSV;
    var line;
    var ticket = data.map(function(obj){
        if(obj["apiData"] && obj["apiData"].declarant1 && obj["apiData"].declarant2){
           line =  fields.map(function (attr) {
               switch (attr){
                   case "demande_id":
                       res = obj["ID"];
                       break;
                   case "first_name":
                       res = obj["apiData"].declarant1.prenoms;
                       break;
                   case "last_name":
                       res = obj["apiData"].declarant1.nomNaissance;
                       break;
                   case "date_naissance":
                       res = obj["apiData"].declarant1.dateNaissance;
                       break;
                   default:
                       res = "";
               }

               res = res.toString();
               if(res.lastIndexOf(',') >= 0){
                   res = '"' + res + '"'
               }
               return res;
           }).join(',');
           line += "\n";
           line += fields.map(function (attr) {
               res = "";
               switch (attr){
                   case "demande_id":
                       res = obj["ID"];
                       break;
                   case "first_name":
                       res = obj["apiData"].declarant2.prenoms;
                       break;
                   case "last_name":
                       res = obj["apiData"].declarant2.nomNaissance;
                       break;
                   case "date_naissance":
                       res = obj["apiData"].declarant2.dateNaissance;
                       break;
                   default:
                       res = "";
               }
               res = res.toString();
               if(res.lastIndexOf(',') >= 0){
                   res = '"' + res + '"'
               }
               return res;
           }).join(',');
           return line;
        }
    });
    CSV = ticket.join("\n");
    return CSV;
}
