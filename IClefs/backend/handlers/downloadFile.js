
function downloadFile(request, response) {

    var res = "fichier n'existe pas !";
    if(!request.urlQuery)
        return res;

    var fileName = getfileName(request.urlQuery); //"CV_Kholti-Hamza.pdf"; //"original.jpg";
    var downloadFolder = Folder('/PROJECT/database/data/tmp/');
    var refFile = new File(downloadFolder.path + fileName);


    if(refFile.exists){
        myBinaryStream = BinaryStream(refFile, 'Read');
        var size = myBinaryStream.getSize();
        res = myBinaryStream.getBlob(size);
        myBinaryStream.close();
        //response.headers['content-type'] = getContentType(fileName);
    }

    return res;
}

// recuperer le nom de fichier a partir de url
function getfileName(str) {

    var tab = str.split("=");

    return tab[1];
}

// détecter le type de fichier (jpg,png,txt,pdf et doc)
function getContentType(fileName) {

    var extention = fileName.split(".")[1];
    var contentType = "";
    switch(extention) {
        case "jpg": contentType = "application/CSV"; break;
        case "png": contentType = "application/CSV"; break;
        case "pdf": contentType = "application/CSV"; break;
        case "doc": contentType = "text/plain"; break;
        case "docx": contentType = "text/plain"; break;
        case "txt": contentType = "application/CSV"; break;
        default: contentType = "none";
    }
    return contentType;
}

