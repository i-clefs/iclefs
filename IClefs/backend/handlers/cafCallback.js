var mailTicket = require("../modules/emails/ticket/ticket.request.js");
var Constantes = require("../modules/const.js");

function CafCallBack (request, response) {
    var bodyRequest = JSON.parse(request.body);
    InfoApi = {
        scope: "caf",
        scope2: "famille",
        codePostal: bodyRequest.codePostal,
        numeroAllocataire: bodyRequest.numeroAllocataire
    };
    var ID = 0;
    if(sessionStorage.getItem("fcData") === null){
        response.body = JSON.stringify({"messageText" : "identifiant pivote n'est pas reçu", "ticketID" : 0});
        return response;
    }
    var token = currentSession().promoteWith(Constantes.agent);
    try {
        var res = getInfoApiParticulier(InfoApi);
        if (res.status === false){
            response.body = JSON.stringify({"messageText" : res.msg, "ticketID" : 0});
            return response;
        }

        sessionStorage.setItem("cafData",res.obj);
        var service = ds.Services.find("ID = :1",bodyRequest.refService);

        if(!service){
            response.body = JSON.stringify({"messageText" : "service n'existe pas", "ticketID" : 0});
            return response;
        }
        if(!(service.personaliseData.length > 0)){
                var newTicket = new ds.Ticket();
                newTicket.service = service;
                newTicket.collectivity = newTicket.service.collectivity;
                newTicket.fcData = sessionStorage.getItem("fcData");
                if(sessionStorage.getItem("apiData"))
                    newTicket.apiData = sessionStorage.getItem("apiData");
                newTicket.cafData = sessionStorage.getItem("cafData");
                newTicket.service.save();
                newTicket.date = new Date();
                newTicket.status = false;
                newTicket.archive = false;
                newTicket.save();
                res.msg = "ticket saved";
                sessionStorage.clear();
                ID = newTicket.ID;
                var r = mailTicket.sendEmail(newTicket, service.collectivity.name, service.name);

            }

        response.statusCode = res.statusCode;
        response.body = JSON.stringify({"messageText" : res.msg, "ticketID" : ID});
        currentSession().unPromote(token);
        return response;
    }
    catch (e) {
		currentSession().unPromote(token);
        response.statusCode = 400;
        response.message = "Une erreur est survenue";
    }
}

getInfoApiParticulier = function(Info)
{
    var res = {
        status: false,
        statusCode  : 0,
        msg: "",
        obj:{}
    }
    var xhr = new XMLHttpRequest();

    var url = "https://particulier-sandbox.api.gouv.fr/api/" + Info.scope + "/" + Info.scope2+ "?" + "codePostal=" + Info.codePostal + "&numeroAllocataire=" + Info.numeroAllocataire;


    // changer pour la vrai URL
    var response;
    xhr.open("GET", url);
    xhr.setRequestHeader('Content-type', 'application/json');

    xhr.setRequestHeader('X-API-KEY', "test-token"); // changer pour le vrai token
    xhr.setRequestHeader('X-USER', "demo"); // changer pour le vrai USER

    xhr.onreadystatechange = function(event)
    {
        res.statusCode = xhr.status;
        if (xhr.readyState == 4 && xhr.status == 200){
            response = JSON.parse(xhr.responseText);

        }
        else if (xhr.status != 200)
        {
            res.msg = xhr.responseText
            if(xhr.responseText){
                response = JSON.parse(xhr.responseText);
            }
        }
    }
    xhr.send();
    if (response)
    {
        if(response.error != null){
            res.msg = response.reason;
        }
        else
        {
            res.status = true;
            res.obj = response;
        }
    }
    return res;
}



