/*
  this handler it just for initialising some session variable to do some UnitTest
  */

function setSession(request, response){
    var body = JSON.parse(request.body);
    var res = {
        status : true,
        msg : ""
    };
    var key = body.key;
    var value = body.value;
    if(key === null || key === "" || value === null || value === ""){
        response.body = JSON.stringify({"messageText" : "paramètre erroné"});
        return response;
    }
    try{
        sessionStorage.setItem(key,value);
        response.body = JSON.stringify({"messageText" : "attribut est bien ajouté"});
    }
    catch (e){
        //response.statusCode = 500;
        response.body = JSON.stringify({"messageText" : "attribut n'est pas ajouté"});
    }
    return response;
}
