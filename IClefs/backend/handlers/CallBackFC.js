var fcTools = require("../modules/oauth2-provider-FranceConnect");
var Constantes = require("../modules/const.js");
var mailTicket = require("../modules/emails/ticket/ticket.request.js");
var countriesList = require("../modules/countries.json");

function callBackFC(request, response) {
    var res = {
        msg: "",
        status: false,
        redirect: ""
    }
    var code = "";
    var state = "";
    var redirectURL = "";
    var uri = Constantes.uri;
	var token = currentSession().promoteWith(Constantes.agent);

    try {
        if ((code = request.parts[0].asText) == null ||
            (state = request.parts[1].asText) == null) {
            res.msg = "url don't have the code or the state or the both";
            res.status = false;
            return (res);
        }
        var serviceIdx = ds.Services.find("ID = :1", state);

        var allInfo = fcTools.exchangeCodeForToken(code , state, serviceIdx.collectivity.client_Id, serviceIdx.collectivity.client_secret);

        if (allInfo.status == false) {
            res.msg = allInfo.msg;
            return (res);
        }
        var allInfo = fcTools.getUserInfo(allInfo);

        if (serviceIdx.status == false) {
            response.headers["Location"] = uri+"/app/#!/service_inactive";
            return;
        }
        allInfo.postRedirectUri = serviceIdx.collectivity.postRedirectUri;
        var fcData = getplaceCountry(allInfo.userInfo);
        sessionStorage.setItem("fcData",fcData);
        res.msg = "callBackFc OK";
        res.status = true;

        response.statusCode = 302;
        if((serviceIdx.config.avisImposition) || (serviceIdx.config.adresseFiscale)){
            redirectURL = uri+"/app/#!/dgfip?state="+serviceIdx.ID;
        }
        else if((serviceIdx.config.adresse) || (serviceIdx.config.quotientFamilial) || (serviceIdx.config.compositionFamiliale)){
            redirectURL = uri+"/app/#!/caf?srv="+serviceIdx.ID;
        }
        else if(serviceIdx.personaliseData.length > 0){
            redirectURL = uri+"/app/#!/personalisedData?srv="+serviceIdx.ID+"&ticket="+9999;
        }
        else{
            var newTicket = new ds.Ticket();
            newTicket.service = serviceIdx;
            newTicket.collectivity = newTicket.service.collectivity;
            newTicket.fcData = sessionStorage.getItem("fcData");
            newTicket.apiData = {};
            newTicket.service.save();
            newTicket.date = new Date();
            newTicket.status = false;
            newTicket.archive = false;
            newTicket.save();
            sessionStorage.clear();
            var r = mailTicket.sendEmail(newTicket, serviceIdx.collectivity.name, serviceIdx.name);
            redirectURL = uri+"/app/#!/endRequest?srv="+serviceIdx.ID+"&ticket="+newTicket.ID;
        }
    }
    catch (e) {
    	currentSession().unPromote(token);
        res.msg = e.message;
        redirectURL = uri+"/app/#!/error_service";
    }
    finally {
//    	stateMutex.unlock();
		currentSession().unPromote(token);
        response.headers["Location"]  = redirectURL;
        
    }
}


function getplaceCountry(fcData){
    var tmp = fcData;
    if(tmp.birthplace !== ""){
        var url = "https://geo.api.gouv.fr/communes?code="+tmp.birthplace;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", url);
        var response;
        xhr.onreadystatechange = function(event){
            if (xhr.readyState == 4 && xhr.status == 200){
                response = JSON.parse(xhr.responseText);
                tmp.birthplace = response[0].nom;
            }
        }
        xhr.send();
    }
    if(tmp.birthcountry !== "")
        tmp.birthcountry = findCountry(parseInt(tmp.birthcountry));
    return tmp;
}

function findCountry(code) {
    var T = countriesList.length;
    var name = "";
    for(var i=0;i<T;i++){
        if(countriesList[i].code === code){
            name = countriesList[i].name;
            break;
        }

    }
    return name;
}