﻿var Constantes = require("../const.js");

exports.getAuthorizedUrl = function getAuthorizedUrl(fcClientID, fcClientSecret, requetID)
{
    var res = {
        status: false,
        msg: "",
        obj:
            {}
    };
    var uri = Constantes.uri;

    var client = {
        "authorize": "https://fcp.integ01.dev-franceconnect.fr/api/v1/authorize",
        "conf": "openid%20profil%20birth%20email%20address%20phone%20identite_pivot",
        "client_id": fcClientID,
        "client_secret": fcClientSecret,
        "redirect_uri": uri+"/api/v1/fc/callback",
        "baseUrl": uri+"/api/v1/fc/callback",
        "scope": "openid",
        "authentication": true
    };
    if (fcClientID == null || fcClientSecret == null)
    {
        res.msg = "Bad value in arguments"
        return res;
    }

    var boutton = "<div id=\"FC_button\"><a href=\"";
    var url;
    url = client.authorize + "?response_type=code&client_id=" + client.client_id;
    url = url + "&redirect_uri=" + client.redirect_uri;
    url = url + "&scope=" + client.conf;
    url = url + "&state=" + requetID;
    url = url + "&nonce=" + generateUUID();
    boutton = boutton + url + "\"><img src=\"https://franceconnect.gouv.fr/images/fc_bouton_v2.png\"></a></div>"
    res.msg = "getAuthorizedUrl OK";
    res.status = true;
    res.obj = {
        request: boutton,
        data: url
    };
    return res;
}


exports.exchangeCodeForToken = function exchangeCodeForToken(code, state, client_Id, client_secret)
{
    var res = {
        accessToken: "",
        idToken: "",
        tokenType: "",
        time: ""
    };

    var xhr = new XMLHttpRequest();
    var uri = Constantes.uri;
    var body = JSON.stringify(
        {
            'code': code,
            'client_id': client_Id,
            'client_secret': client_secret,
            'redirect_uri': uri+"/api/v1/fc/callback",
            'grant_type': 'authorization_code'
        });
    xhr.open('POST', 'https://fcp.integ01.dev-franceconnect.fr/api/v1/token', false);
    xhr.setRequestHeader('Content-type', 'application/json');
    try
    {
        xhr.send(body);
    }
    catch (e)
    {
        res.msg = e;
        res.status = false;
        return (res);
    }
    finally
    {
        var reponse = JSON.parse(xhr.responseText);
        if (reponse.error != null)
        {
            res.status = false;
            res.msg = reponse.reason;
            return res;
        }
        res.accessToken = reponse.access_token;
        res.idToken = reponse.id_token;
        res.tokenType = reponse.token_type;
        res.time = reponse.expires_in;
        res.state = state;
        res.status = true;
        res.msg = "exchangeCodeForToken OK"
        return res;
    }
}

exports.getUserInfo = function(Info)
{
    var reponse;
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "https://fcp.integ01.dev-franceconnect.fr/api/v1/userinfo?schema=openid", false);
    xhr.setRequestHeader("Authorization", Info.tokenType + " " + Info.accessToken);
    xhr.onreadystatechange = function()
    {
        if (xhr.readyState == 4 && xhr.status == 200)
            reponse = JSON.parse(xhr.responseText);
        else if (xhr.status != 200)
            reponse = JSON.parse(xhr.responseText);
    }
    xhr.send();
    if (reponse.error != null)
    {
        Info.status = false;
        Info.msg = "getUserInfo : error :  " + reponse.reason;
        return (Info);
    }
    else
    {
        Info.userInfo = reponse;
        Info.status = true;
        Info.msg = "getUserInfo OK";
        return (Info);
    }
}

exports.logoutUser = function(Info)
{
    var res = {
        msg: "",
        status: false,
        obj: {}
    }
    var url = "https://fcp.integ01.dev-franceconnect.fr/api/v1/logout?";
    url = url + "id_token_hint=" + Info.idToken;
    url = url + "&state=" + Info.state;
    url = url + "&post_logout_redirect_uri=" + Info.postRedirectUri; // changer par le site collectivité
    res.obj.data = url;
    res.status = true;
    res.msg = "logoutUser OK";
    return (res);

}
