var mail = require("../../../modules/sendMail.js");
var _ = require("../../../modules/lodash.js");

exports.sendEmail = function(user, serviceName, collectivityName){
    var subject = "Le création du service "+serviceName+" a bien été effectuée";
    var T = _.template(loadText("/PROJECT/backend/modules/emails/service/service.addService.html"));
    console.log("load template body");
    var message = T({'name'  :   [user.firstName],
                     'serviceName' :   [serviceName],
                     'collectivityName' :   [collectivityName]});
    var res = mail.send(subject, message, user.userName);

    return res.status;
}