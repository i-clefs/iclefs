var mail = require("../../../modules/sendMail.js");
var _ = require("../../../modules/lodash.js");

exports.sendEmail = function(ticket, collectivityName, serviceName){
    var subject = "Demande service en ligne";
    var T = _.template(loadText("/PROJECT/backend/modules/emails/ticket/ticket.request.html"));
    var message = T({'name'  :   [ticket.fcData.given_name],
        'serviceName' :   [serviceName],
        'collectivityName'  :   [collectivityName],
        'num'   :   [ticket.ID]});
    var res = mail.send(subject, message, ticket.fcData.email);

    return res.status;
}