var mail = require("../../../../modules/sendMail.js");
var _ = require("../../../../modules/lodash.js");

exports.sendEmail = function(user, collectivityName){
    var subject = "Votre demande a été validée";
    var T = _.template(loadText("/PROJECT/backend/modules/emails/collectivity/validateRequest/collectivity.validateRequest.html"));
    var message = T({'name'  :   [user.firstName],
        'collectivityName' :   [collectivityName]});
    var res = mail.send(subject, message, user.userName);

    return res.status;
}