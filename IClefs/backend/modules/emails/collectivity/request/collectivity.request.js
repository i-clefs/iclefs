var mail = require("../../../../modules/sendMail.js");
var _ = require("../../../../modules/lodash.js");

exports.sendEmail = function(user, collectivityName){
    var subject = "Votre demande de création de la collectivité est en cours de traitement";
    var T = _.template(loadText("/PROJECT/backend/modules/emails/collectivity/request/collectivity.request.html"));
    var message = T({'name'  :   [user.firstName],
        'collectivityName' :   [collectivityName]});
    var res = mail.send(subject, message, user.userName);

    return res.status;
}