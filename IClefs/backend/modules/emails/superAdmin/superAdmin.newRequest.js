var mail = require("../../../modules/sendMail.js");
var _ = require("../../../modules/lodash.js");

exports.sendEmail = function(admin, userName, collectivityName){
    var subject = "Nouvelle Demande";
    var T = _.template(loadText("/PROJECT/backend/modules/emails/superAdmin/superAdmin.newRequest.html"));
    var message = T({'name'  :   [admin.firstName],
                    'userName' :   [userName],
                    'collectivityName' :   [collectivityName]});
    var res = mail.send(subject, message, admin.userName);

    return res.status;
}