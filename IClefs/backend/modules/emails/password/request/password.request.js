var mail = require("sendMail.js");
var _ = require("lodash.js");
var Constantes = require("const.js");

exports.sendEmail = function(user, token){
    var subject = "Réinitialisation de votre mot de passe !";
    var T = _.template(loadText("/PROJECT/backend/modules/emails/password/request/password.request.html"));
    var uri = Constantes.uri;
    var url = uri+"/app/#!/resetPassword/"+token;
    var message = T({'name'  :   [user.firstName],
                    'url' :   [url]});
    var res = mail.send(subject, message, user.userName);

    return res.status;
}