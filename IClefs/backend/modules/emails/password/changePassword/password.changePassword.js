var mail = require("../../../../modules/sendMail.js");
var _ = require("../../../../modules/lodash.js");

exports.sendEmail = function(user){
    var subject = "Votre mot de passe a été modifié";
    var T = _.template(loadText("/PROJECT/backend/modules/emails/password/changePassword/password.changePassword.html"));
    var message = T({'name'  :   [user.firstName]});
    var res = mail.send(subject, message, user.userName);

    return res.status;
}