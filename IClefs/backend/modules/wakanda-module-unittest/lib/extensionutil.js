function _runUnitTestInExtension (path, url, projectpath, automatic) {
    try{
        if (typeof path !== 'undefined' && typeof url !== 'undefined' && typeof projectpath !== 'undefined') {
            window.currentProjectBasePath = projectpath;
            window._waktest_waf_ready = function() {
                _waktestRun();
            };
            window.waktest_ended = function(data) {
                if (typeof studio !== 'undefined' && typeof studio.sendCommand === 'function') {
                    studio.sendCommand('wakanda-extension-unittest.wakbot_any', JSON.stringify({report: data, event: 'waktest_ended', kind: 'extension', automatic: automatic}));
                }
                return data;
            };
            var waktestScript = document.createElement('script');
            waktestScript.type = 'application/javascript';
            waktestScript.src = url + '/waktest-waf-lib?path=' + path + '&widgetId=WakandaExtension';
            document.getElementsByTagName('head')[0].appendChild(waktestScript);
        }
    } catch (ignore) {}
}

if (typeof window._wakRunUnitTestArgs !== 'undefined') {
    _runUnitTestInExtension.apply(this, window._wakRunUnitTestArgs);
}