var _ = require("./lodash.js");
var Constantes = require("./const.js");

exports.send = function (subject, body, usernameTo){
    var res = {
        status : false,
        msg : ""
    };
    var mail = require('waf-mail/mail');
    var text = loadText("/PROJECT/backend/modules/emails/template/template.email.html");
    var T = _.template(text);

    if(subject === "" || subject === null || body === "" || body === null || usernameTo === "" || usernameTo === null){
        res.msg = "paramètre erroné";
        return res;
    }
    if(usernameTo.indexOf('@') === -1){
        res.msg = "Adresse mail non valide";
        return res;
    }
    var message = new mail.Mail();
    message.subject = subject;
    message.from = Constantes.usernameFrom;
    message.to = [usernameTo];
    message.setBodyType ('text/html');
    message.setBody(T({'body'   :   body,
        'url'   :   [Constantes.logoURL]}));

    var result = mail.send({
        address: Constantes.smtpAddress,
        port: 465,
        isSSL: true,
        username: Constantes.usernameFrom,
        password: Constantes.password,
        domain: Constantes.smtpdomain
    }, message);
    if(!result.isOk){
        res.msg = "mail non envoyé";
        return res;
    }
    res.status = result.isOk;
    res.msg = "mail envoyé";

    return res;
}
