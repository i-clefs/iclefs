/*
 * A BootStrap is a JavaScript file that is run when your project is loaded on the server.
 * You can use it  to initialize your application, define HTTP pattern handlers etc..
 */

directory.setLoginListener("myLogin", "Admin");
addHttpRequestHandler("/api/v1/fc/callback", "./backend/handlers/CallBackFC.js", "callBackFC");

addHttpRequestHandler("/api/v1/dgfip/callback", "./backend/handlers/dgfipCallback.js", "DgfipCallBack");
addHttpRequestHandler("/api/v1/caf/callback","./backend/handlers/cafCallback.js","CafCallBack");

addHttpRequestHandler("/api/v1/request/cancelRequest","./backend/handlers/cancelRequest.js","cancelRequest");
addHttpRequestHandler("/api/v1/data/exportCSV","./backend/handlers/exportCSV.js","generateCSV");



addHttpRequestHandler("/api/v1/service/uploadFile","./backend/handlers/uploadFile.js","uploadFile");
addHttpRequestHandler("/api/v1/service/downloadFile","./backend/handlers/downloadFile.js","downloadFile");
addHttpRequestHandler("/createSuperAdmin","./backend/Untitled1.js","createSuperAdmin");


addHttpRequestHandler("/setSessionStorage","./backend/handlers/setSession.js","setSession");